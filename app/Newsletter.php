<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Newsletter extends Model
{
    protected $fillable=[
    	'email','first_name','last_name','phone_number'
];
}
