<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Schema;
use Illuminate\Http\Request;
use App\Blog;
use App\Menu;
use App\Service;
use App\Slider;
use App\User;
use App\PageSetting;
use App\Language;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $dlanguage = '1';
        Language::truncate(); 
        $selected = new Language();
        $selected->selected_language = $dlanguage;
        $selected->save();
        
        $pageSetting = PageSetting::findOrFail('1');
        $users = User::count();
        $menus = Menu::count();
        $services = Service::count();
        $blogs = Blog::count();
        $slider = Slider::count();
        return view('home',compact('users','menus','services','blogs','pageSetting','slider','dlanguage'));
    }
    public function selectedIndex(Request $request,$id)
    {
        $dlanguage = '1';

        if($id){
            Language::truncate(); 
            $selected = new Language();
            $selected->selected_language = $id;
            $selected->save();
        }
        $pageSetting = PageSetting::findOrFail('1');
        $users = User::count();
        $menus = Menu::count();
        $services = Service::count();
        $blogs = Blog::count();
        $slider = Slider::count();
        return view('home',compact('users','menus','services','blogs','pageSetting','slider','dlanguage'));
    }
    // public function language()
    // {
    //     Language::truncate();
    //     $menus = Menu::all();
    //     return view('language',compact('menus'));
    // }
}
