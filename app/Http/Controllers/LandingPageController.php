<?php

namespace App\Http\Controllers;

use App\landingPage;
use Illuminate\Http\Request;

class LandingPageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $selectedl = session()->get('mlanguage');
        $selectedP = landingPage::where('language_id',$selectedl)->get();
        if(count($selectedP)){
        $page = $selectedP[0];
        return view ('dashboard.landing.edit',compact('page'));
        }
        else{
        return view ('dashboard.landing.new');
    }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $page = new landingPage();
        $page->service_details = $request->service_details;
        $page->team_details = $request->team_details;
        $page->signup_details = $request->signup_details;
        if(file_exists($request->file('signup_image1'))){
            $image = $request->file('signup_image1');
            $imageName =  "signup_image1".time().'.'.$request->file('signup_image1')->getClientOriginalName();
            $image->move(public_path('uploads/landing'),$imageName);
            $page->signup_image1 = $imageName;
        }
        if(file_exists($request->file('signup_image2'))){
            $image = $request->file('signup_image2');
            $imageName =  "signup_image2".time().'.'.$request->file('signup_image2')->getClientOriginalName();
            $image->move(public_path('uploads/landing'),$imageName);
            $page->signup_image2 = $imageName;
        }
        $page->video_text = $request->video_text;
        if(file_exists($request->file('lower_video'))){
            $file = "landing".time().'.'.$request->file('lower_video')->getclientOriginalExtension();
            $location = public_path('uploads/landing');
            $request->file('lower_video')->move($location, $file);
            $page->lower_video = $file;
        }
        $page->otp_video = $request->otp_video;
        $page->otp_text = $request->otp_text;
        $page->text1 = $request->text1;
        $page->text2 = $request->text2;
        $page->text3 = $request->text3;
        $page->text4 = $request->text4;
        $page->language_id = $request->language_id;

        $page->save();
        return redirect('/backoffice/landing');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\landingPage  $landingPage
     * @return \Illuminate\Http\Response
     */
    public function show(landingPage $landingPage)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\landingPage  $landingPage
     * @return \Illuminate\Http\Response
     */
    public function edit(landingPage $landingPage)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\landingPage  $landingPage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, landingPage $landingPage,$id)
    {
        $page = landingPage::findOrFail($id);
        if($request->service_details){
        $page->service_details = $request->service_details;
        }
        else{
         $page->service_details = $page->service_details;   
        }
        if($request->team_details){
        $page->team_details = $request->team_details;
        }
        else{
         $page->team_details = $page->team_details;   
        }
        if($request->signup_details){
        $page->signup_details = $request->signup_details;
        }
        else{
         $page->signup_details = $page->signup_details;   
        }
        if(file_exists($request->file('signup_image1'))){
            $image = $request->file('signup_image1');
            $imageName =  "signup_image1".time().'.'.$request->file('signup_image1')->getClientOriginalName();
            $image->move(public_path('uploads/landing'),$imageName);
            $page->signup_image1 = $imageName;
        }
        else{
         $page->signup_image1 = $page->signup_image1;   
        }
        if(file_exists($request->file('signup_image2'))){
            $image = $request->file('signup_image2');
            $imageName =  "signup_image2".time().'.'.$request->file('signup_image2')->getClientOriginalName();
            $image->move(public_path('uploads/landing'),$imageName);
            $page->signup_image2 = $imageName;
        }
        else{
         $page->signup_image2 = $page->signup_image2;   
        }
        if($request->video_text){
        $page->video_text = $request->video_text;
        }
        else{
         $page->video_text = $page->video_text;   
        }
        if(file_exists($request->file('lower_video'))){
            $file = "landing".time().'.'.$request->file('lower_video')->getclientOriginalExtension();
            $location = public_path('uploads/landing');
            $request->file('lower_video')->move($location, $file);
            $page->lower_video = $file;
        }
        else{
         $page->lower_video = $page->lower_video;   
        }
        if($request->otp_video){
        $page->otp_video = $request->otp_video;
        }
        else{
         $page->otp_video = $page->otp_video;   
        }
        if($request->otp_text){
        $page->otp_text = $request->otp_text;
        }
        else{
         $page->otp_text = $page->otp_text;   
        }
        if($request->text1){
        $page->text1 = $request->text1;
        }
        else{
         $page->text1 = $page->text1;   
        }
        if($request->text2){
        $page->text2 = $request->text2;
        }
        else{
         $page->text2 = $page->text2;   
        }
        if($request->text3){
        $page->text3 = $request->text3;
        }
        else{
         $page->text3 = $page->text3;   
        }
        if($request->text4){
        $page->text4 = $request->text4;
        }
        else{
         $page->text4 = $page->text4;   
        }
        if($request->language_id){
         $page->language_id = $request->language_id;
        }
        else{
        $page->language_id = $page->language_id;
        }
        $page->save();
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\landingPage  $landingPage
     * @return \Illuminate\Http\Response
     */
    public function destroy(landingPage $landingPage)
    {
        //
    }
}
