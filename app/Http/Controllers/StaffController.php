<?php

namespace App\Http\Controllers;

use App\staff;
use App\landingPage;
use Illuminate\Http\Request;

class StaffController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $selectedl = session()->get('mlanguage');
        $selectedP = landingPage::where('language_id',$selectedl)->get();
        if($selectedl){
        $jobDetail = $selectedP[0];
    }
        $staffs = Staff::where('language_id',$selectedl)->get();
        return view('dashboard.staffs.index', compact('staffs','jobDetail'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.staffs.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $staffs = new Staff();
        $request->validate([
            'name' => 'required',
            'description' => 'required',
            'image' => 'image|mimes:jpg,png,jpeg'
        ]);
        $staffs->name =$request->name;
        $staffs->description =$request->description;
        $staffs->language_id =$request->language_id;
        if(file_exists($request->file('image'))){
            $image = "staffs".time().'.'.$request->file('image')->getclientOriginalName();
            $location = public_path('uploads');
            $request->file('image')->move($location, $image);
            $staffs->image = $image;
        }
        else{
            $staffs->image = 'default-thumbnail.png';
        }
        $staffs->save();
        return redirect('/backoffice/products');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\staff  $staff
     * @return \Illuminate\Http\Response
     */
    public function show(staff $staff)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\staff  $staff
     * @return \Illuminate\Http\Response
     */
    public function edit(staff $staff, $id)
    {
        $staffs = Staff::findOrFail($id);
        return view ('dashboard.staffs.edit',compact('staffs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\staff  $staff
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, staff $staff,$id)
    {
        $staffs = Staff::findOrFail($id);
        $request->validate([
            'name' => 'required',
            'description' => 'required',
            'image' => 'image|mimes:jpg,png,jpeg'
        ]);
        $staffs->name =$request->name;
        $staffs->description =$request->description;
        $staffs->language_id =$request->language_id;
        $staffs->mail =$request->mail;
        if(file_exists($request->file('image'))){
            $image = "staffs".time().'.'.$request->file('image')->getclientOriginalName();
            $location = public_path('uploads');
            $request->file('image')->move($location, $image);
            $staffs->image = $image;
        }
        else{
            $staffs->image = $staffs->image;
        }
        $staffs->save();
        return redirect('backoffice/products');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\staff  $staff
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $staffs = Staff::findOrFail($id)->delete();
    return redirect()->back();
    }
}
