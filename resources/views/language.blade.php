<!-- <!DOCTYPE html>
<html lang="en">
<head>
  <title>Language Select</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <style type="text/css">
    .marque-text{
      display: inline-block;
      overflow: hidden;
      text-align: initial;
      white-space: nowrap;
      text-transform: capitalize;
      font-size: 20px;
    }
    span.spacer {
      content: '';
      padding: 0px 80px;
    }
    .Language-select-first{
      background-color:lavender;
      padding:150px 200px;
      margin: auto;
      font-size: 24px;
      font-weight: 600;
    }
    .Language-select{
      background-color:lavenderblush;
      padding:150px 200px;
      margin: auto;
      font-size: 24px;
      font-weight: 600;
    }
    .col-sm-4.Language-select:hover {
      background: #2a81f4;
      color: white;
    }
    .col-sm-4.Language-select-first:hover {
      background: #2a81f4;
      color: white;
    }
    .links a {
    color: black;
    text-decoration: none;
    font-size: 16px;
}
  </style>
</head>
<body>

  <div class="container-fluid">
    <h1 style="text-align: center;">Hello Admin!</h1>
    <marquee onmouseover="this.stop();" onmouseout="this.start();">
      <p class="marque-text">Choose the Language To Continue To Dashboard.</p>
      <span class="spacer"></span>
      <p class="marque-text">Pilih Bahasa Untuk Terus Ke Papan Pemuka.</p>
      <span class="spacer" style="width: 50px"></span>
      <p class="marque-text">ड्यासबोर्डमा जारी राख्न भाषा चयन गर्नुहोस्।</p>
    </marquee>
    <div class="links">
      <a href="/">< Go back</a> /
<a href="{{ route('logout') }}"onclick="event.preventDefault();document.getElementById('logout-form').submit();">
<i class="zmdi zmdi-power"></i>{{ __('Sign out') }}
</a>
<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
@csrf
</form>
    </div>
    <div class="row">
      <?php $count=1; ?>
      @foreach($menus as $menu)
      @if($count%2 == 0)
      <a href="{{$menu->url}}/{{$menu->id}}"><div class="col-sm-4 Language-select-first"> {{$menu->label}} </div></a>
      @else
      <a href="{{$menu->url}}/{{$menu->id}}"><div class="col-sm-4 Language-select"> {{$menu->label}} </div></a>
      @endif
      <?php $count ++; ?>
      @endforeach
    </div>
  </div>

</body>
</html>
 -->
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Landing page</title>
          <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }
            .full-height {
                height: 100vh;
            }
            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }
            .position-ref {
                position: relative;
            }
            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }
            .content {
                text-align: center;
            }
            .title {
                font-size: 34px;
            }
            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }
            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/') }}">Home</a>
                        <a href="{{ route('logout') }}"onclick="event.preventDefault();document.getElementById('logout-form').submit();">
<i class="zmdi zmdi-power"></i>{{ __('Sign out') }}
</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>
                    @endauth
                </div>
            @endif

<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
@csrf
</form>

            <div class="content">
                <div class="title m-b-md">
                    Welcome to Dashboard
                </div>
                <div class="title m-b-md">
                    Selamat datang ke Papan Pemuka
                </div>
                <div class="title m-b-md">
                    ड्यासबोर्डमा स्वागत छ |
                </div>
                <div class="title m-b-md">
                    Selamat datang di Dasbor
                </div>
                <div class="title m-b-md">
                   ダッシュボードへようこそ
                </div>

                <div class="links">
                    <form action="/backoffi" method="GET" id="myForm">
                      @csrf
                      <div class="row form-group">
                      <label class="control-label text-capitalize">Select the language:</label>
                      <select class="form-control" onchange="goToNext()" name="selected_language">
                        <option>---</option>
                        @foreach($menus as $menu)
                        <option value="{{$menu->id}}">{{$menu->label}}</option>
                        @endforeach
                      </select>
                    </div>
                    </form>
                </div>
            </div>
        </div>
        <script type="text/javascript">
          function goToNext(){
            document.getElementById('myForm').submit();
          }
        </script>
    </body>
</html>