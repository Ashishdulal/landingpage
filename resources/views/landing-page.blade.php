 @extends('layouts.new.app', ['title' => 'Landing Page'],['discription'=> ($pageSetting->tagline)])
 @section('metaDescription')
 <meta name="tagline" content="{{$pageSetting->tagline}}">
 <meta name="description" content="{{$pageSetting->meta_description_seo}}">
 <meta name="site url" content="{{$pageSetting->site_url}}">
 <meta name="keywords" content="{{$pageSetting->meta_keywords_seo}}">
 @endsection
 @section('customCss')
 <link rel="stylesheet" href="/css/intlTelInput.css">
 <link rel="stylesheet" href="/css/demo.css">
 <style type="text/css">
  .rd-navbar-aside-outer.rd-navbar-collapse.toggle-original-elements {
    display: none;
  }
  header.section.page-header {
   height: auto !important; 
 }
 .social-block.my-footer-link{
  display: none;
}
</style>
@endsection

@section('content')
<!--<p>
{{ __('welcome.welcome') }} \\Output, Herzlich willkommen
 
{{ __('welcome.demo') }} \\Output, Wie geht es dir?
</p> -->
@if($pageD!=1)
<section class="section swiper-container swiper-slider swiper-slider-2 slider-scale-effect" data-loop="false" data-autoplay="5500" data-simulate-touch="false" data-slide-effect="fade"> 
  <div class="swiper-wrapper">
   @foreach($sliders as $slider)
   @if($slider->language_id == (Session::get('mainlanguage')))
   <div class="swiper-slide">
    <div class="slide-bg" style="background-image: url(&quot;/uploads/{{$slider->image}}&quot;)"></div>
    <div class="swiper-slide-caption section-md">
     <div class="container">
      <div class="row">
       <div class="col-sm-10 col-lg-7 col-xl-6 swiper-caption-inner">
        <h1 data-caption-animate="fadeInUp" data-caption-delay="100"><span class="text-primary">{{$slider->title}}</span><br>{{$slider->title_1}}
        </h1>
        <div class="divider-lg" data-caption-animate="fadeInLeft" data-caption-delay="550"></div>
        <p class="lead" data-caption-animate="fadeInUp" data-caption-delay="250"><?php echo ($slider->description)?></p>
        <a class="button button-default-outline" data-toggle="modal" data-target="#mySignup" href="#" data-caption-animate="fadeInUp" data-caption-delay="450">Sign Up</a>
      </div>
    </div>
  </div>
</div>
</div>
@endif
@endforeach

</div>
<!-- Swiper Pagination -->
<div class="swiper-pagination"></div>
<div class="swiper-button-prev"></div>
<div class="swiper-button-next"></div>
</section>
<!-- The Modal -->
@if($showOtpModal == 1)
<div class="modal fade-in" style="display: block;" id="mySignup">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content make-other">

      <!-- Modal Header -->
      <div class="modal-header my-modal">
        <div class="intro_form_title">Join</div>
        <button type="button" class="close" onclick="dissapearModal()" data-dismiss="modal">&times;</button>
      </div>
      <div class="col-lg-12 intro_col hov_form">
        @if (count($errors) > 0)
        <div class="alert alert-danger">
         <button type="button" class="close" data-dismiss="alert">×</button>
         <ul>
          @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
      @endif
      @if ($message = Session::get('success'))
      <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>{{ $message }}</strong>
      </div>
      @endif
      <!-- RD Mailform-->
      @if($showOOtp == 1)
      <div style="padding-top: 20px;" class="pb-5">
        <div class="col-sm-12 text-center">
          <h3>Please Enter The Otp Code You Received</h3><br>
          <div class="divider-lg"></div>
        </div>
      </div>
      <form class="rd-form-inline" method="post" action="{{url('/otp-code')}}">
        @csrf
        <div class="col-sm-6">
          <div class="form-wrap">
            <label>Request-id:</label>
            <input type="text" name="request_id" class="form-input" readonly value="{{ session()->get('Code')}}">
          </div>
        </div>
        <div class="col-sm-6">
          <div class="form-wrap">
            <label>Otp Code:</label>

            <input id="otp" type="number" class="form-input" name="otp_code" required placeholder="Otp Code">
          </div>
        </div>
        <div style="padding:30px" class="form-button1 verify">
          <button class="button button-primary" type="submit">Verify</button>
        </div>
      </form>
      @endif
    </div>
    <!-- Modal footer -->
    <div class="modal-footer">
      <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> -->
      <form class="rd-form-inline" method="post" action="{{url('/cancel')}}">
        @csrf
        <div class="form-wrap">
          <input id="request_id" type="hidden" class="form-input" name="request_id" required value="{{ session()->get('Code')}}">
        </div>
        <div class="form-button1 verify">
          <button class="button button-primary" type="submit">Cancel</button>
        </div>
      </form>
    </div>

  </div>
</div>
</div>
@endif
<div class="modal fade" id="mySignup">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content make-other">

      <!-- Modal Header -->
      <div class="modal-header my-modal">
        <div class="intro_form_title">Join</div>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="col-lg-12 intro_col hov_form">
        @if (count($errors) > 0)
        <div class="alert alert-danger">
         <button type="button" class="close" data-dismiss="alert">×</button>
         <ul>
          @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
      @endif
      @if ($message = Session::get('success'))
      <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>{{ $message }}</strong>
      </div>
      @endif
      <!-- RD Mailform-->
      <form class="text-left" action="{{url('/otp')}}" method="post" >
        @csrf
        <input type="hidden" name="subject" value="New Register/Landing Page">
        <input type="hidden" name="message" value="Get The new member Registered">
        <div class="row row-15">
         <div class="col-sm-6">
          <div class="form-wrap">
           <input class="form-input" id="contact-name" type="text" name="first_name" placeholder="First name" required>
         </div>
       </div>
       <div class="col-sm-6">
        <div class="form-wrap">
         <input class="form-input" id="contact-sec-name" type="text" name="last_name" placeholder="Last name" required>
       </div>
     </div>
     <div class="col-sm-6">
      <div class="form-wrap outer-phn">
        <input type="tel" class="form-input" id="phone" name="phone_number" required="">
      </div>
    </div>
    <div class="col-sm-6">
      <div class="form-wrap">
       <input class="form-input" id="contact-email" type="email" name="email" placeholder="E-Mail" required>
     </div>
   </div>
 </div>
 <div class="form-button group-sm text-left">
   <button class="button button-primary" onclick="showOtp()" type="submit">Join</button>
 </div>
</form>
<div id="otpCodeDisplay" style="display: none;">
  <div style="padding-top: 20px;" class="pb-5">
    <div class="col-sm-12 text-center">
      <h3>Please Enter The Otp Code You Received</h3><br>
      <div class="divider-lg"></div>
    </div>
  </div>
  <form class="rd-form-inline" method="post" action="{{url('/otp-code')}}">
    @csrf
    <div class="col-sm-6">
      <div class="form-wrap">
        <label>Request-id:</label>
        <input type="text" name="request_id" class="form-input" readonly value="{{ session()->get('Code')}}">
      </div>
    </div>
    <div class="col-sm-6">
      <div class="form-wrap">
        <label>Otp Code:</label>

        <input id="otp" type="number" class="form-input" name="otp_code" required placeholder="Otp Code">
      </div>
    </div>
    <div style="padding:30px" class="form-button1 verify">
      <button class="button button-primary" type="submit">Verify</button>
    </div>
  </form>
</div>
</div>
<!-- Modal footer -->
<div class="modal-footer">
  <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> -->
  <form class="rd-form-inline" method="post" action="{{url('/cancel')}}">
    @csrf
    <div class="form-wrap">
      <input id="request_id" type="hidden" class="form-input" name="request_id" required value="{{ session()->get('Code')}}">
    </div>
    <div class="form-button1 verify">
      <button class="button button-primary" type="submit">Cancel</button>
    </div>
  </form>
</div>

</div>
</div>
</div>
<script type="text/javascript">
 function dissapearModal(){
  // alert('hello');
  document.getElementById("mySignup").className = "otp-modal-change";
}
</script>

<section id="haruyosi_about" class="section section-md bg-default text-center">
  <div class="container">
    <?php echo ($page->service_details)?>
    <div class="row row-30">
      <div class="col-12">
        <!-- Owl Carousel-->
        <div class="owl-carousel carousel-creative" data-items="1" data-lg-items="3" data-dots="true" data-nav="false" data-stage-padding="15" data-loop="false" data-margin="30" data-mouse-drag="false">
          @foreach($services as $service)
          @if($service->language_id == (Session::get('mainlanguage')))
          <div class="team-minimal team-minimal-with-shadow">
            <figure style="background-color: #f6f6f6;border-radius: 50%;"><img src="/uploads/{{$service->image}}"></figure>
            <div class="team-minimal-caption">
              <h4 class="team-title">{{$service->name}}</h4>
            </div>
            <?php echo ($service->description)?>
          </div>
          @endif
          @endforeach

        </div>
      </div>
      <!--       <div class="col-12"><a class="button button-default-outline" data-toggle="modal" data-target="#myModal" href="#">Schedule a demo</a></div> -->
      <!-- <div class="col-12"><a class="button button-default-outline" href="/our-team">View all team</a></div> -->
    </div>
  </div>
</section>
<section id="haruyosi_about" class="section section-md bg-default text-center">
  <div class="container">
    <?php echo ($page->team_details)?>
    <div class="row row-30">
      <div class="col-12">
        <!-- Owl Carousel-->
        <div class="owl-carousel carousel-creative" data-items="1" data-lg-items="3" data-dots="true" data-nav="false" data-stage-padding="15" data-loop="false" data-margin="30" data-mouse-drag="false">
          @foreach($staffs as $staff)
          @if($staff->language_id == (Session::get('mainlanguage')))
          <div class="team-minimal team-minimal-with-shadow">
            <figure><img src="/uploads/{{$staff->image}}" alt="" width="370" height="370"></figure>
            <div class="team-minimal-caption">
              <h4 class="team-title">{{$staff->name}}</h4>
            </div>
            <?php echo ($staff->description)?>
          </div>
          @endif
          @endforeach

        </div>
      </div>
      <!-- <div class="col-12"><a class="button button-default-outline" href="/our-team">View all team</a></div> -->
    </div>
  </div>
</section>
<section class="section section-md" id="contact-form">
  <div class="container">
   <div class="row row-50">
    <div class="col-lg-6">
      <?php echo ($page->signup_details)?>

      <!-- RD Mailform-->
      <form class="text-left" action="{{url('/otp')}}" method="post" >
        @csrf
        <input type="hidden" name="subject" value="New Register/Landing Page">
        <input type="hidden" name="message" value="Get The new member Registered">
        <div class="row row-15">
         <div class="col-sm-6">
          <div class="form-wrap">
           <input class="form-input" id="contact-name" type="text" name="first_name" placeholder="First name" value="{{old('first_name')}}"required>
         </div>
       </div>
       <div class="col-sm-6">
        <div class="form-wrap">
         <input class="form-input" id="contact-sec-name" type="text" name="last_name" placeholder="Last name" value="{{old('last_name')}}" required>
       </div>
     </div>
     <div class="col-sm-6">
      <div class="form-wrap outer-phn">
        <input type="tel" class="form-input" id="phonea" name="phone_number" required="">
      </div>
    </div>
    <div class="col-sm-6">
      <div class="form-wrap">
       <input class="form-input" id="contact-email" type="email" name="email" placeholder="E-Mail" required>
     </div>
   </div>
 </div><br>
 @if (count($errors) > 0)
 <div class="alert alert-danger">
  <button type="button" class="close" data-dismiss="alert">×</button>
  <ul>
   @foreach ($errors->all() as $error)
   <li>{{ $error }}<br></li>
   @endforeach
 </ul>
</div>
@endif
@if ($message = Session::get('success'))
<div class="alert alert-success alert-block">
  <button type="button" class="close" data-dismiss="alert">×</button>
  <strong>{{ $message }}</strong>
</div>
@endif
<div class="form-button group-sm text-left">
 <button class="button button-primary" type="submit">Signup</button>
</div>
</form>
<div id="otpCodeDisplay" >
  @if($showOOtp == 1)
  @include('otp')
  @endif
</div>
</div>
<script type="text/javascript">
 function showOtp(){
  document.getElementById("otpCodeDisplay").className = "otp-code-display";
}
</script>
<div class="col-lg-6">
 <div class="box-images box-images-variant-3">
  <div class="box-images-item" data-parallax-scroll="{&quot;y&quot;: -20,   &quot;smoothness&quot;: 30 }">
    <img src="/uploads/landing/{{$page->signup_image1}}" alt="" width="470" height="282"/>
  </div>
  <div class="box-images-item box-images-without-border" data-parallax-scroll="{&quot;y&quot;: 40,  &quot;smoothness&quot;: 30 }"><img src="/uploads/landing/{{$page->signup_image2}}" alt="" width="470" height="282"/>
  </div>
</div>
</div>
</div>
</div>
</section>
<section class="section-md video-bg-overlay bg-vide" data-vide-bg="mp4: /uploads/landing/{{$page->lower_video}}, poster: images/bg-video" data-vide-options="posterType: jpg, position: 0% 50%">
  <div style="position: absolute; z-index: 0; top: 0px; left: 0px; bottom: 0px; right: 0px; overflow: hidden; background-size: cover; background-color: transparent; background-repeat: no-repeat; background-position: 0% 50%; background-image: none;"><video autoplay="" loop="" muted="" style="margin: auto; position: absolute; z-index: -1; top: 50%; left: 0%; transform: translate(0%, -50%); visibility: visible; opacity: 1; width: 1888px; height: auto;" __idm_id__="683964417"><source src="/uploads/landing/{{$page->lower_video}}" type="video/mp4"></video></div>
    <div class="section-sm context-dark text-center">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-md-10">
            <div class="pb-5">
              <?php echo ($page->video_text)?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <script src="/js/intlTelInput.js"></script>
  <script>
    var input = document.querySelector("#phone");
    window.intlTelInput(input, {
      initialCountry: "auto",
    preferredCountries: [],
      geoIpLookup: function(callback) {
        $.get('https://ipinfo.io', function() {}, "jsonp").always(function(resp) {
          var countryCode = (resp && resp.country) ? resp.country : "";
          callback(countryCode);
        });
      },
  utilsScript: "/js/utils.js?1562189064761" // just for formatting/placeholders etc
});
</script>
<script>
  var input = document.querySelector("#phonea");
  window.intlTelInput(input, {
    initialCountry: "auto",
  preferredCountries: [],
    geoIpLookup: function(callback) {
      $.get('https://ipinfo.io', function() {}, "jsonp").always(function(resp) {
        var countryCode = (resp && resp.country) ? resp.country : "";
        callback(countryCode);
      });
    },
  utilsScript: "/js/utils.js?1562189064761" // just for formatting/placeholders etc
});
</script>
@else
<h3 style="text-align: center;padding: 100px 0;text-transform: uppercase;">No Contents To Display</h3>
@endif
@endsection