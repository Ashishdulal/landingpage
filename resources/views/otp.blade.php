

<style type="text/css">
	.rd-navbar-aside-outer.rd-navbar-collapse.toggle-original-elements {
		display: none;
	}
	header.section.page-header {
		height: auto !important; 
	}
	.social-block.my-footer-link{
		display: none;
	}
	.form-wrap {
		max-width: 100%;
	}
	.rd-form-inline .form-input {
		width: 100%;
		overflow: hidden;
		float: left;
		padding-right: 5% !important;
	}
	.verify button:hover {
		border-color: #0052cc !important;
	}
	.otp-code-display{
		display: block !important;
	}
</style>
<div style="padding: 30px 0" class="col-md-10">
	<div class="pb-5">
		<div class="col-sm-12 text-center">
			<h3>Please Enter The Otp Code You Received</h3><br>
			<div class="divider-lg"></div>
		</div>
	</div>
	@if (count($errors) > 0)
	<div class="alert alert-danger">
		<button type="button" class="close" data-dismiss="alert">×</button>
		<ul>
			@foreach ($errors->all() as $error)
			<li>{{ $error }}<br></li>
			@endforeach
		</ul>
	</div>
	@endif
	@if ($message = Session::get('success'))
	<div class="alert alert-success alert-block">
		<button type="button" class="close" data-dismiss="alert">×</button>
		<strong>{{ $message }}</strong>
	</div>
	@endif
	<form style="display: inline-flex; float: left; padding: 0 0 0 30px;" class="rd-form-inline" method="post" action="{{url('/otp-code')}}">
		@csrf
		<div class="make-gap">
			<div class="form-wrap">
				<label>Request-id:</label>
				<input type="text" name="request_id" class="form-input" readonly value="{{ session()->get('Code')}}">
			</div>
		</div>
		<div class="make-gap"><br>
			<div class="form-wrap">
				<label>Otp Code:</label>

				<input id="otp" type="number" class="form-input" name="otp_code" required placeholder="Otp Code">
			</div>
		</div>
		<div style="padding:0 40px" class="form-button1 verify">
			<button class="button button-primary" type="submit">Verify</button>
		</div>
	</form>
	<form style="margin-top: 15.5%;" class="rd-form-inline" method="post" action="{{url('/cancel')}}">
		@csrf
		<div class="form-wrap">
			<input id="request_id" type="hidden" class="form-input" name="request_id" required value="{{ session()->get('Code')}}">
		</div>
		<div class="form-button1 verify">
			<button class="button button-primary" type="submit">Cancel</button>
		</div>
	</form>
</div>
