 <style type="text/css">
  .rd-navbar-aside-outer.rd-navbar-collapse.toggle-original-elements {
    display: none;
  }
  header.section.page-header {
   height: auto !important; 
 }
 .social-block.my-footer-link{
  display: none;
}
</style>
    <div class="page">
      <!-- Page Header-->
      <header class="section page-header">
        <!-- RD Navbar-->
        <div class="rd-navbar-wrap">
          <nav class="rd-navbar rd-navbar-classic rd-navbar-classic-minimal" data-layout="rd-navbar-fixed" data-sm-layout="rd-navbar-fixed" data-md-layout="rd-navbar-fixed" data-md-device-layout="rd-navbar-fixed" data-lg-layout="rd-navbar-static" data-lg-device-layout="rd-navbar-static" data-xl-layout="rd-navbar-static" data-xl-device-layout="rd-navbar-static" data-lg-stick-up-offset="46px" data-xl-stick-up-offset="46px" data-xxl-stick-up-offset="46px" data-lg-stick-up="true" data-xl-stick-up="true" data-xxl-stick-up="true">
            <div class="rd-navbar-aside-outer rd-navbar-collapse toggle-original-elements">
              <div class="rd-navbar-aside">
                <div class="header-info">
                  <ul class="list-inline list-inline-md">
                    <li>
                      <div class="unit unit-spacing-xs align-items-center">
                        <div class="unit-left font-weight-bold">Free Call:</div>
                        <div class="unit-body"><a href="tel:abcd">abcd</a></div>
                      </div>
                    </li>
                    <li>
                      <div class="unit unit-spacing-xs align-items-center">
                        <div class="unit-left font-weight-bold">Opening Hours: </div>
                        <div class="unit-body"> abcd</div>
                      </div>
                    </li>
                  </ul>
                </div>
                <div class="social-block">
                  <ul class="list-inline">
                    <li><a class="icon fa-facebook" target="_blank" href="abcd"></a></li>
                    <li><a class="icon fa-twitter" target="_blank" href="abcd"></a></li>
                    <li><a class="icon fa-instagram" target="_blank" href="abcd"></a></li>
                    <li><a class="icon fa-linkedin" target="_blank" href="abcd"></a></li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="rd-navbar-main-outer">
              <div class="rd-navbar-panel">
                    <!-- RD Navbar Toggle-->
                  <button class="mobile-globe fas fa-globe rd-navbar-toggle" data-rd-navbar-toggle=".rd-navbar-nav-wrap"><span></span></button>
                  <div class="rd-navbar-brand">
                    <a class="brand" href="/"><img src="/uploads/homepage/{{$pageSetting->site_logo}}" alt="Logo" width="158" height="58"/></a>
                  </div>
                    <!-- <div id="google_translate_element"></div> -->
            </div>
          </div>
            <div class="rd-navbar-main-element">
                  <div class="rd-navbar-nav-wrap">
                    <!-- RD Navbar Nav-->
                    <ul class="rd-navbar-nav">
                      <?php 
                      use App\Menu;
                      $primarymenus = Menu::where('parent_id', null)->orderBy('order', 'ASC')->get();
                      $selectedl = session()->get('mainlanguage');
                      if($selectedl){
                      $selectedMenu = Menu::where('id',$selectedl)->first();
                    }
                        foreach($primarymenus as $pmenu){
                            ?>
                      <span class="menu-icon {{ request()->is($pmenu->id) ? 'active' : ''}} {{ Request::is('/') ? 'active' : '' }} fas fa-globe"></span><li class="rd-nav-item {{ request()->is($pmenu->id) ? 'active' : ''}}  {{ Request::is('/') ? 'active' : '' }}">
                        <a class="rd-nav-link" href="/language/{{ $selectedMenu->id }}">{{$selectedMenu->label}}</a>

                            <?php 
                            $secondarymenus = Menu::where('parent_id', $pmenu->id)->get();
                            if(count($secondarymenus)){?>
                                <ul class="rd-menu rd-navbar-dropdown">
                              @if($selectedMenu->id != 1)
                                  <li class="rd-dropdown-item {{ request()->is($pmenu->id) ? 'active' : ''}}{{ Request::is('/') ? 'active' : '' }}">
                                    <a class="rd-dropdown-link" href="/language/{{ $pmenu->id }}">{{ $pmenu->label }}</a>
                                  </li>
                                  @endif
                            <?php foreach($secondarymenus as $secm){ ?>
                                  <li class="rd-dropdown-item {{ request()->is($secm->id) ? 'active' : ''}}">
                                    <a class="rd-dropdown-link" href="/language/{{ $secm->id }}">{{ $secm->label }}</a>

                                  </li>  
                          <?php } ?>
                                </ul>                                 
                          <?php } ?>
                      </li>
                    <?php      
                        }
                    ?>
                      </ul>
                  </div>
                </div>
          </nav>
        </div>
      </header>