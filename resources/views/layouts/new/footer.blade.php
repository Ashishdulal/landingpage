@if($pageSetting->footer_visibility == 1)
<footer class="section bg-default section-xs-type-1 footer-minimal">
  <div class="container">
    <?php echo ($pageSetting->footer_text) ?>
<!--     <div class="row row-30 align-items-lg-center justify-content-lg-between">
      <div class="col-lg-2">
        <div class="footer-brand"><a href="index.html"><img src="images/logo-dark-142x58.png" alt="" width="142" height="58"/></a></div>
      </div>
      <div class="col-lg-10">
        <div class="footer-nav">
          <ul class="rd-navbar-nav">
            <li class="rd-nav-item active"><a class="rd-nav-link" href="index.html">Home</a></li>
            <li class="rd-nav-item"><a class="rd-nav-link" href="overview.html">About</a></li>
            <li class="rd-nav-item"><a class="rd-nav-link" href="services.html">Services</a></li>
            <li class="rd-nav-item"><a class="rd-nav-link" href="grid-gallery.html">Gallery</a></li>
            <li class="rd-nav-item"><a class="rd-nav-link" href="blog-classic.html">Blog</a></li>
            <li class="rd-nav-item"><a class="rd-nav-link" href="#">Pages</a></li>
            <li class="rd-nav-item"><a class="rd-nav-link" href="contacts.html">Contacts</a></li>
          </ul>
        </div>
      </div>
    </div> -->
  </div>
</footer>
@endif
<section class="bg-gray-100 section-xs text-center">
  <div class="social-block my-footer-link">
    <ul class="list-inline">
      <li><a class="icon fa-facebook" target="_blank" href="abcd"></a></li>&nbsp;&nbsp;
      <li><a class="icon fa-twitter" target="_blank" href="abcd"></a></li>&nbsp;&nbsp;
      <li><a class="icon fa-instagram" target="_blank" href="abcd"></a></li>&nbsp;&nbsp;
      <li><a class="icon fa-linkedin" target="_blank" href="abcd"></a></li>&nbsp;&nbsp;
    </ul>
  </div>
  <div class="container">
    <?php echo ($pageSetting->copyright_text) ?>
    <!--       <p class="rights">Copyright<span>&copy;&nbsp; </span><span class="copyright-year"></span><span>&nbsp;</span><span>All Rights Reserved by<a href="http://www.nepgeeks.com" target="_blank"> Nepgeeks Technology</a></span></p> -->
  </div>
</section>
