@extends('layouts.dashboard.app')

@section('content')
<!-- PAGE CONTAINER-->
<div class="col-lg-12">
	<div class="card">
		<div class="card-header">
			<div class="au-breadcrumb-left">
				<span class="au-breadcrumb-span">You are here:</span>
				<ul class="list-unstyled list-inline au-breadcrumb__list">
					<li class="list-inline-item">
						<a href="/backoffice">Dashboard</a>
					</li>
					<li class="list-inline-item seprate">
						<span>/</span>
					</li>
					<li class="list-inline-item active">Landing Page Content</li>
				</ul>
			</div>
		</div>
		@if($errors->any())
		@foreach($errors->all() as $error)
		<ul>
			<li>{{$error}}</li>
		</ul>
		@endforeach
		@endif
		<div class="card-body card-block">
			<span class="au-breadcrumb-span note-space">[Note: <a href="#help_image">Click For Help</a>]</span>
			@if($page->language_id == (Session::get('mlanguage')))
			<form action="/landingpage/{{$page->id}}" method="POST" class="form-horizontal" enctype="multipart/form-data">
				@csrf
				<input type="hidden" name="language_id" value="{{ Session::get('mlanguage') }}">
				
				<button style="float: right;" type="submit" class="btn btn-primary btn-sm">
					<i class="fa fa-dot-circle-o"></i> Update
				</button>
				<br>
				<br>
				<div class="row form-group">
					<div class="col col-md-3">
						<label for="text2" class=" form-control-label">Otp API Key</label>
					</div>
					<div class="col-12 col-md-9">
						<input type="name" id="text2" name="text2" value="{{$page->text2}}" class="form-control">
						<span class="au-breadcrumb-span">[ Note: Change the Otp API Code only.]</span>

					</div>
				</div>
				<div class="row form-group">
					<div class="col col-md-3">
						<label for="text3" class=" form-control-label">Otp API Secret Key</label>
					</div>
					<div class="col-12 col-md-9">
						<input type="name" id="text3" name="text3" value="{{$page->text3}}" class="form-control">
						<span class="au-breadcrumb-span">[ Note: Change the Otp API Secret Key only.]</span>

					</div>
				</div>
				<div class="row form-group">
					<div class="col col-md-3">
						<label for="signup_details" class=" form-control-label">SignUp Description (1)</label>
						<span class="au-breadcrumb-span">[ Note: To add line copy this image <p><img src="/images/line.png" /></p>]</span>
					</div>
					<div class="col-12 col-md-9">
						<textarea name="signup_details"  rows="9"  class="form-control ckeditor">{{$page->signup_details}}</textarea>
					</div>
				</div>
				<div class="row form-group">
					<div class="col col-md-3">
						<label for="signup_image1" class=" form-control-label">SignUp First Image (2)</label><br>
						<span class="au-breadcrumb-span">[ Note: Please upload the image size 470*282 and should be less than 100kb ]</span>
					</div>
					<div class="col-12 col-md-9 process">
						<input type="file" onchange="readURL(this);" id="signup_image1" accept="image/png, image/jpg, image/jpeg" name="signup_image1" class="form-control-file">
						<img id="blah" src="/uploads/landing/{{$page->signup_image1}}" alt="image">
					</div>
				</div>
				<div class="row form-group">
					<div class="col col-md-3">
						<label for="signup_image2" class=" form-control-label"> SignUp Second Image (3)</label><br>
						<span class="au-breadcrumb-span">[ Note: Please upload the image size 470*282 and should be less than 100kb ]</span>
					</div>
					<div class="col-12 col-md-9 process">
						<input type="file" onchange="readURL1(this);" id="signup_image2" accept="image/png, image/jpg, image/jpeg" name="signup_image2" class="form-control-file">
						<img id="blah1" src="/uploads/landing/{{$page->signup_image2}}" alt="Image">
					</div>
				</div>
				<div class="row form-group">
					<div class="col col-md-3">
						<label for="video_text" class=" form-control-label">Video Title (4)</label>
						<span class="au-breadcrumb-span">[ Note: To add line copy this image <p><img src="/images/line.png" /></p>]</span>
					</div>
					<div class="col-12 col-md-9">
						<textarea name="video_text"  rows="9"  class="form-control ckeditor">{{$page->video_text}}</textarea>
					</div>
				</div>
				<div class="row form-group">
					<div class="col col-md-3">
						<label for="lower_video" class=" form-control-label">Video (5)</label><br>
						<span class="au-breadcrumb-span">[ Note: Please upload the video less than 5-10mb ]</span>
					</div>
					<div class="col-12 col-md-9">
						<input type="file" name="lower_video" class="form-control-file file_multi_video" accept="video/mp4,video/avi,video/mpeg,video/MKV">

						<video style="margin-top: 20px;" width="400" controls>
							<source src="/uploads/landing/{{$page->lower_video}}" id="video_here">
								Your browser does not support HTML5 video.
							</video>
						</div>
					</div>
				<div class="row form-group">
					<div class="col col-md-3">
						<label for="text1" class=" form-control-label">Chat Details (6)</label>
					</div>
					<div class="col-12 col-md-9">
						<input type="name" id="text1" name="text1" value="{{$page->text1}}" class="form-control">
						<span class="au-breadcrumb-span">[ Note: Change the fafa class and link only. In default:<br> [ class="icon fa-whatsapp chat-logo" target="_blank" href="https://wa.me/15551234567" ] ]</span>

					</div>
				</div>
				<button type="submit" class="btn btn-primary btn-sm">
					<i class="fa fa-dot-circle-o"></i> Update
				</button>
			</form>
			@endif
		</div>
		<div id="help_image" class="card-footer">
			<span class="au-breadcrumb-span">[The below Numbers in the image denotes upper number for changes.]  </span><br><br
			<div class="row">
				<!-- <div class="col-md-6"> -->
					<div class="col-md-12 help-image">
						<img src="/images/landing-page-1.jpg" alt="Image">
					</div>
				</div>
			</div>
		</div>
	</div>



</div><!--/.col-->

@endsection