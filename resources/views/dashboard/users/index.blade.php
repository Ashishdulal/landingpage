@extends('layouts.dashboard.app')

@section('content')
<!-- PAGE CONTAINER-->
<div class="col-lg-12">
	<div class="card">
		<div class="card-header">
			<div class="au-breadcrumb-left">
				<span class="au-breadcrumb-span">You are here:</span>
				<ul class="list-unstyled list-inline au-breadcrumb__list">
					<li class="list-inline-item">
						<a href="/backoffice">Home</a>
					</li>
					<li class="list-inline-item seprate">
						<span>/</span>
					</li>
					<li class="list-inline-item active">Users</li>
				</ul>
			</div>
		</div>
		<div class="row m-t-30">
			<div class="col-md-12">
				<a style="margin: 0 0 20px 20px;" href="/backoffice/users/create" class="btn btn-primary">Add New User</a>
				<!-- DATA TABLE-->
				@if ($errors->any())
				<div class="alert alert-danger">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
					<ul>
						@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
				@endif

				@if (Session::has('success'))
				<div class="alert alert-success text-center">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
					<p>{{ Session::get('success') }}</p>
				</div>
				@endif
				<div class="table-responsive m-b-40">
					<table class="table table-borderless table-data3">
						<table class="table table-hover">
							<tr>
								<th>S. NO.</th>
								<th>Name</th>
								<th>Email Address</th>
								<th>Image<br><span class="small-text">[ Click image to change it. ]</span></th>
								<th>Action</th>
							</tr>

							@foreach ($users as $user)
							<tr>
								<td>{{ $loop->iteration }}</td>
								<td>{{$user->name}}</td>
								<td>{{$user->email}}</td>
								<td>
									<div style="max-width: 50%;cursor:pointer;" class="admin-img">
										<img data-toggle="modal" data-target="#myModalImage{{$user->id}}" style="max-width: 50%;" src="/images/admin/{{$user->image}}" alt="Click To Change The profile Pic">
									</div>
								</td>
								<td>
									<!-- <a href="/backoffice/users/edit/{{$user->id}}"> <button class="btn btn-info make-btn">Edit</button></a> | -->
									<form method="delete" action="{{route('delete.user',$user->id)}}"> 

										{{csrf_field()}}

										{{method_field('DELETE')}}
										<div class="field"><button class="btn btn-danger" onclick="makeWarning(event)">Delete</button></div>
									</form>

								</td>
							</tr>
							<!-- Modal -->
							<div class="modal fade" id="myModalImage{{$user->id}}" role="dialog">
								<div class="modal-dialog">

									<!-- Modal content-->
									<div class="modal-content">
										<div class="modal-header">
											<h4 class="modal-title">User Profile Pic</h4>
											<button type="button" class="close" data-dismiss="modal">&times;</button>
										</div>
										<div class="modal-body">
											<form method="post" action="/backoffice/users/image/{{$user->id}}" enctype="multipart/form-data"> 

												@csrf
												<div class="field">
													<!-- image input-->
													<div class="form-group">
														<label class="control-label" for="image">Select Image:</label>
														<span class="au-breadcrumb-span">[ Please upload the image size 600*600 and size less than 100kb ]</span>
														<div class="">
															<input type="file" onchange="readURL(this);" class="form-control" name="image"  accept="image/png, image/jpg, image/jpeg">
															<br><img id="blah" src="/images/admin/{{$user->image}}" alt="Selected Image" />
														</div>
													</div>
													<button style="float: left;" type="submit" class="btn btn-primary">submit</button></div>
													<a href="#" style="float: left;margin-left: 10px;" class="btn btn-danger" data-dismiss="modal">Close</a>
												</form>
											</div>
											<div class="modal-footer">
											</div>
										</div>

									</div>
								</div>
								@endforeach
							</table>
						</div>
						<div class="card-footer">

						</div>
					</div>
				</div>



			</div><!--/.col-->
			<script type="text/javascript">
				function makeWarning(evt){
					let result = confirm("Are you sure to Delete?");
					if(! result){
						evt.stopPropagation();
						evt.preventDefault();	
					}
				}
			</script>
			@endsection