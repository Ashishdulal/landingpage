@extends('layouts.dashboard.app')

@section('content')
<!-- PAGE CONTAINER-->
<div class="col-lg-12">
	<div class="card">
		<div class="card-header">
			<div class="au-breadcrumb-left">
				<span class="au-breadcrumb-span">You are here:</span>
				<ul class="list-unstyled list-inline au-breadcrumb__list">
					<li class="list-inline-item">
						<a href="/backoffice">Home</a>
					</li>
					<li class="list-inline-item seprate">
						<span>/</span>
					</li>
					<li class="list-inline-item active">
						<a href="/backoffice/services">services</a>
					</li>
				</ul>
			</div>
		</div>
		<div class="row m-t-30">
			<div class="col-md-12 dashboard-space">
				<h4>Service Details</h4><br>
				<div class="job-banner-image">
					<a href="#" data-toggle="modal" data-target="#myBanner"><span style="color: #000000;text-align: center;"><?php echo ($serviceDetail->service_details) ?></span><br><span style="margin: 20px 0 0 20px;" class="btn btn-primary">Edit Banner Details</span></a>
				</div>
				<!-- Modal -->
				<div class="modal fade" id="myBanner" role="dialog">
					<div class="modal-dialog">
						
						<!-- Modal content-->
						<div class="modal-content">
							<div class="modal-header">
								<h4 class="modal-title">Team Details</h4>
								<button type="button" class="close" data-dismiss="modal">&times;</button>
							</div>
							<div class="modal-body">
								<form method="post" action="/landingpage/{{$serviceDetail->id}}" enctype="multipart/form-data"> 

									@csrf
								<input type="hidden" name="language_id" value="{{ Session::get('mlanguage') }}">

									<div class="field">
										<!-- image input-->
										<div class="row form-group">
													<div class="col col-md-12">
														<label for="service_details" class=" form-control-label">Team Description:</label>
														<span class="au-breadcrumb-span">[ Note: To add line copy this image <p><img src="/images/line.png" /></p>]</span>
													</div>
													<div class="col-12 col-md-12">
														<textarea name="service_details"  rows="9"  class="form-control ckeditor">{{$serviceDetail->service_details}}</textarea>
														
													</div>
												</div>
										<button style="float: left;" type="submit" class="btn btn-primary">submit</button></div>
										<a href="#" style="float: left;margin-left: 10px;" class="btn btn-danger" data-dismiss="modal">Close</a>
									</form>
								</div>
								<div class="modal-footer">
								</div>
							</div>
							
						</div>
					</div>
					<br>
					<a style="margin: 0 0 20px 20px ;" href="/backoffice/services/create" class="btn btn-primary">Add New Service</a>
					<!-- DATA TABLE-->
					<div class="table-responsive m-b-40">
						<table class="table table-borderless table-data3">
							<thead>
								<tr>
									<th>Id</th>
									<th>Name</th>
									<th>Description</th>
									<th>Image</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								@if(count($services))
								@foreach($services as $service)
								<tr>
									<td>{{ $loop->iteration }}</td>
									<td>{{$service->name}}</td>
									<td><?php echo ($service->description)?></td>
									<td class="process"><img src="/uploads/{{$service->image}}"></td>
									<td class="make_btn_straight"><a href="{{route('service.edit', $service->id)}}"><button class="btn btn-primary make-btn">Edit</button></a> &nbsp;|&nbsp;
										<form method="post" action="{{route('service.delete',$service->id)}}">
											@csrf
											{{ method_field('DELETE') }}
											<button type="submit" onclick="makeWarning(event)" class="btn btn-danger">Delete</button>
										</form>
									</td>
								</tr>
								@endforeach
								@else
								<tr><td style="text-align:center;padding:10px 0px; text-transform: uppercase;" colspan="7"><h2>No Services Added</h2></td></tr>
								@endif
							</tbody>
						</table>
					</div>
					<!-- END DATA TABLE-->
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	function makeWarning(evt){
		let result = confirm("Are you sure to Delete?");
		if(! result){
			evt.stopPropagation();
			evt.preventDefault();	
		}
	}
</script>

@endsection