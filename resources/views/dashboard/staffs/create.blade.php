@extends('layouts.dashboard.app')

@section('content')
<!-- PAGE CONTAINER-->
<div class="col-lg-12">
	<div class="card">
		<div class="card-header">
			<div class="au-breadcrumb-left">
				<span class="au-breadcrumb-span">You are here:</span>
				<ul class="list-unstyled list-inline au-breadcrumb__list">
					<li class="list-inline-item">
						<a href="/backoffice">Home</a>
					</li>
					<li class="list-inline-item seprate">
						<span>/</span>
					</li>
					<li class="list-inline-item">
						<a href="/backoffice/products">Products</a>
					</li>
					<li class="list-inline-item seprate">
						<span>/</span>
					</li>
					<li class="list-inline-item active">create</li>
				</ul>
			</div>
		</div>
		@if($errors->any())
		@foreach($errors->all() as $error)
		<ul>
			<li>{{$error}}</li>
		</ul>
		@endforeach
		@endif
		<div class="card-body card-block">
			<form class="form-horizontal" action="/backoffice/products/create" method="post" enctype="multipart/form-data">
				@csrf
				<input type="hidden" name="language_id" value="{{ Session::get('mlanguage') }}">
				<fieldset>
					<!-- Name input-->
					<div class="form-group">
						<label class="col-md-3 control-label" for="name">Products Name:</label>
						<div class="col-md-9">
							<input id="ch_name" name="name" type="text" placeholder="Products Name"  class="form-control">
						</div>
					</div>
						
					<div class="form-group">
						<label class="col-md-3 control-label" for="image">Description:</label>
						<div class="col-md-9">
							<textarea id="description" name="description" type="text" required="" placeholder="Your Description" class="form-control ckeditor"></textarea>
						</div>
					</div>
					<!-- image input-->
					<div class="form-group">
						<label class="col-md-3 control-label" for="image">Select Image:</label>
						<span class="au-breadcrumb-span">[ Note: Please upload the 600*600 size image and should be less ]</span>
						<div class="col-md-9">
							<app-image-upload  >
								<div  class="fileinput">
									<input  type="file" onchange="readURL(this);" name="image"  accept="image/png, image/jpg, image/jpeg">
								</div>
							</app-image-upload>
							<br><img style="max-width: 50%;" id="blah" src="#" alt="Selected Image" />
						</div>
					</div>

					<!-- Form actions -->
					<button type="submit" class="btn btn-primary btn-sm">
						<i class="fa fa-dot-circle-o"></i> Submit
					</button>
					<button type="reset" class="btn btn-danger btn-sm">
						<i class="fa fa-ban"></i> Reset
					</button>
				</fieldset>
			</form>
		</div>
	</div>
</div>
<div class="copyright">
</div>
</div><!--/.col-->

@endsection