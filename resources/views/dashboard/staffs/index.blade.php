@extends('layouts.dashboard.app')

@section('content')
<!-- PAGE CONTAINER-->


<div class="col-lg-12">
	<div class="card">
		<div class="card-header">
			<div class="au-breadcrumb-left">
				<span class="au-breadcrumb-span">You are here:</span>
				<ul class="list-unstyled list-inline au-breadcrumb__list">
					<li class="list-inline-item">
						<a href="/backoffice">Home</a>
					</li>
					<li class="list-inline-item seprate">
						<span>/</span>
					</li>
					<li class="list-inline-item active">
						<a href="/backoffice/products">Products</a>
					</li>
				</ul>
			</div>
		</div>
		<div class="row m-t-30">
			<div class="col-md-12 dashboard-space">
				<h4>Product Details</h4><br>
				<div class="job-banner-image">
					<a href="#" data-toggle="modal" data-target="#myBanner"><span style="color: #000000;text-align: center;"><?php echo ($jobDetail->team_details) ?></span><br><span style="margin: 20px 0 0 20px;" class="btn btn-primary">Edit Banner Details</span></a>
				</div>
				<!-- Modal -->
				<div class="modal fade" id="myBanner" role="dialog">
					<div class="modal-dialog">
						
						<!-- Modal content-->
						<div class="modal-content">
							<div class="modal-header">
								<h4 class="modal-title">Products Details</h4>
								<button type="button" class="close" data-dismiss="modal">&times;</button>
							</div>
							<div class="modal-body">
								<form method="post" action="/landingpage/{{$jobDetail->id}}" enctype="multipart/form-data"> 

									@csrf
									<div class="field">
										<!-- image input-->
										<div class="row form-group">
													<div class="col col-md-12">
														<label for="team_details" class=" form-control-label">Products Description:</label>
													</div>
													<div class="col-12 col-md-12">
														<textarea name="team_details"  rows="9"  class="form-control ckeditor">{{$jobDetail->team_details}}</textarea>
														
													</div>
												</div>
										<button style="float: left;" type="submit" class="btn btn-primary">submit</button></div>
										<a href="#" style="float: left;margin-left: 10px;" class="btn btn-danger" data-dismiss="modal">Close</a>
									</form>
								</div>
								<div class="modal-footer">
								</div>
							</div>
							
						</div>
					</div>
					<br>
			<div class="col-md-12">
				<a href="/backoffice/products/create" class="btn btn-primary dashboard-button">Add New Product</a>
				<!-- DATA TABLE-->
				<div class="table-responsive m-b-40">
					<table class="table table-borderless table-data3">
						<thead>
							<tr>
								<th>Id</th>
								<th>Products Name</th>
								<th>Description</th>
								<th>Image</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
								@if(count($staffs))
							@foreach($staffs as $team)
							<tr>
								<td>{{ $loop->iteration }}</td>
								<td>{{$team->name}}</td>
								<td><?php echo ($team->description)?></td>
								<td class="work-img">
									<img src="/uploads/{{$team->image}}" alt="{{$team->name}}">
								</td>
								<td class="make_btn_straight">
									<a href="products/edit/{{$team->id}}" class="btn btn-primary make-btn">
										Edit
									</a>|
									<form method="post" action="{{route('delete.staff',$team->id)}}">
										@csrf
										{{ method_field('DELETE') }}
										<button type="submit" class="btn btn-danger" onclick="makeWarning(event)">Delete</button>
									</form>
								</td>
							</tr>
							@endforeach
							@else
								<tr><td style="text-align:center;padding:10px 0px; text-transform: uppercase;" colspan="5"><h2>No Products Added</h2></td></tr>
								@endif
						</tbody>
					</table>
				</div>
				<!-- END DATA TABLE-->
			</div>
		</div>
	</div>
</div>
</div>
<script type="text/javascript">
	function makeWarning(evt){
		let result = confirm("Are you sure to Delete?");
		if(! result){
			evt.stopPropagation();
			evt.preventDefault();	
		}
	}
</script>

@endsection