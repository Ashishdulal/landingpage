@extends('layouts.dashboard.app')

@section('content')
<!-- PAGE CONTAINER-->
<div class="col-lg-12">
	<div class="card">
		<div class="card-header">
			<div class="au-breadcrumb-left">
				<span class="au-breadcrumb-span">You are here:</span>
				<ul class="list-unstyled list-inline au-breadcrumb__list">
					<li class="list-inline-item">
						<a href="/backoffice">Home</a>
					</li>
					<li class="list-inline-item seprate">
						<span>/</span>
					</li>
					<li class="list-inline-item">
						<a href="/backoffice/staffs">ssProducts</a>
					</li>
					<li class="list-inline-item seprate">
						<span>/</span>
					</li>
					<li class="list-inline-item active">Edit</li>
				</ul>
			</div>
		</div>
		@if($errors->any())
		@foreach($errors->all() as $error)
		<ul>
			<li>{{$error}}</li>
		</ul>
		@endforeach
		@endif
		<div class="card-body card-block">
			<form action="/backoffice/products/edit/{{$staffs->id}}" method="POST" class="form-horizontal" enctype="multipart/form-data">
				@csrf
				<input type="hidden" name="language_id" value="{{ Session::get('mlanguage') }}">
				<fieldset>
					<!-- Name input-->
					<div class="form-group">
						<label class="col-md-3 control-label" for="name">Products Name:</label>
						<div class="col-md-9">
							<input id="name" name="name" type="text" value="{{$staffs->name}}"  class="form-control">
						</div>
					</div>					
					<div class="form-group">
						<label class="col-md-3 control-label" for="description">Description:</label>
						<div class="col-md-9">
							<textarea id="description" name="description" type="text" required=""  class="form-control ckeditor">{{$staffs->description}}</textarea>
						</div>
					</div>
					<!-- image input-->
					<div class="form-group col-md-6">
						<app-image-upload  >
							<div  class="fileinput">
								<div  class="thumbnail img-raised">
									<img src="/uploads/{{$staffs->image}}" alt="{{$staffs->name }}"/>
								</div>
								<div >
									<label class="control-label" for="image">Select Image:</label>
								</div>
								<input  type="file" name="image"  accept="image/png, image/jpg, image/jpeg">
							</div>
						</app-image-upload>
					</div>

					<!-- Form actions -->
					<button type="submit" class="btn btn-primary btn-sm">
						<i class="fa fa-dot-circle-o"></i> Submit
					</button>
					<button type="reset" class="btn btn-danger btn-sm">
						<i class="fa fa-ban"></i> Reset
					</button>
				</fieldset>
			</form>
		</div>
		<div class="card-footer">

		</div>
	</div>
</div>



</div><!--/.col-->

@endsection