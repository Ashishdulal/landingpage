<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/backoffice/menus', 'MenuController@index');
Route::get('/backoffice/menu/create', 'MenuController@create');
Route::post('/backoffice/menu/create', 'MenuController@store')->name('make.menu');
Route::get('/backoffice/menu/{id}', 'MenuController@show');
Route::get('/backoffice/menu/edit/{id}', 'MenuController@edit');
Route::post('/backoffice/menu/edit/{id}', 'MenuController@update')->name('update.menu');
Route::get('/backoffice/menu/destroy/{id}', 'MenuController@destroy')->name('delete.menu');
Auth::routes();

Route::get('/backoffice', 'HomeController@index');
Route::get('/backoffice/language/{id}', 'HomeController@selectedIndex');


Route::fallback(function() {
    return view('nopage');
});

Route::get('/','MainPageController@landingPage');
Route::get('/language/{id}','MainPageController@newLandingPage');
Route::post('/otp','MainPageController@otp');
Route::get('/ootp','MainPageController@errorReturn');
Route::post('/otp-code','SendMailController@verify');
Route::post('/cancel','SendMailController@cancel');

Route::get('/backoffice/landing','LandingPageController@index');
Route::post('/landingpage/{id}','LandingPageController@update');
Route::post('/backoffice/landingpage/create','LandingPageController@store');

Route::get('/news','MainPageController@blogIndex');
Route::get('/news/category/{id}','MainPageController@blogCategoryPage');
Route::get('/news-detail/{id}','MainPageController@blogDetail')->name('post.show');

Route::post('/sendemail/send', 'SendMailController@send');
Route::post('/sendjob/send', 'SendMailController@sendJob');
Route::post('/subscribe/send', 'SendMailController@subscribe');
Route::post('/backoffice/newsletter', 'SendMailController@newsletter');

Route::get('/backoffice/subscribers', 'NewsletterController@newsletterIndex');
Route::get('/backoffice/subscriber/destroy/{id}', 'NewsletterController@newsletterDestroy');
Route::post('/backoffice/banner/image/{id}', 'JobController@bannerImage');

Route::get('/backoffice/newsletter', 'NewsletterController@index');

Route::get('/backoffice/users', 'UserPasswordController@index');
Route::get('/backoffice/users/create', 'UserPasswordController@create');
Route::post('/backoffice/users/create', 'UserPasswordController@store')->name('make.user');
Route::get('/backoffice/users/{id}', 'UserPasswordController@show');
Route::get('/backoffice/users/edit/{id}', 'UserPasswordController@edit');
Route::post('/backoffice/users/image/{id}', 'UserPasswordController@imageUpdate');
Route::post('/backoffice/users/edit/{id}', 'UserPasswordController@update')->name('update.user');
Route::get('/backoffice/users/destroy/{id}', 'UserPasswordController@destroy')->name('delete.user');

Route::get('/backoffice/services', 'ServiceController@index');
Route::get('/backoffice/services/create', 'ServiceController@create');
Route::post('/backoffice/services/create', 'ServiceController@store');
Route::get('/backoffice/services/edit/{id}', 'ServiceController@edit')->name('service.edit');
Route::post('/backoffice/services/edit/{id}', 'ServiceController@update')->name('service.update');
Route::delete('/backoffice/services/destroy/{id}', 'ServiceController@destroy')->name('service.delete');


Route::get('/backoffice/news-category', 'BlogcategoryController@index');
Route::get('/backoffice/news-category/create', 'BlogcategoryController@create');
Route::post('/backoffice/news-category/create', 'BlogcategoryController@store')->name('make.blog');
Route::get('/backoffice/news-category/{id}', 'BlogcategoryController@show');
Route::get('/backoffice/news-category/edit/{id}', 'BlogcategoryController@edit');
Route::post('/backoffice/news-category/edit/{id}', 'BlogcategoryController@update')->name('update.blog');
Route::get('/backoffice/news-category/destroy/{id}', 'BlogcategoryController@destroy')->name('delete.blog');


Route::get('/backoffice/news', 'BlogController@index');
Route::get('/backoffice/news/create', 'BlogController@create');
Route::post('/backoffice/news/create', 'BlogController@store')->name('make-post.blog');
Route::get('/backoffice/news/{id}', 'BlogController@show');
Route::get('/backoffice/news/edit/{id}', 'BlogController@edit');
Route::post('/backoffice/news/edit/{id}', 'BlogController@update')->name('update-post.blog');
Route::get('/backoffice/news/destroy/{id}', 'BlogController@destroy')->name('delete-post.blog');


Route::get('/backoffice/sliders', 'SliderController@index');
Route::get('/backoffice/slider/create', 'SliderController@create');
Route::post('/backoffice/slider/create', 'SliderController@store');
Route::get('/backoffice/slider/edit/{id}', 'SliderController@edit')->name('slider.edit');
Route::post('/backoffice/slider/edit/{id}', 'SliderController@update')->name('slider.update');
Route::delete('/backoffice/slider/destroy/{id}', 'SliderController@destroy')->name('slider.delete');


// Route::get('/backoffice/testimonials', 'TestimonialController@index');
// Route::get('/backoffice/testimonials/create', 'TestimonialController@create');
// Route::post('/backoffice/testimonials/create', 'TestimonialController@store');
// Route::get('/backoffice/testimonials/edit/{id}', 'TestimonialController@edit')->name('testimonial.edit');
// Route::post('/backoffice/testimonials/edit/{id}', 'TestimonialController@update')->name('testimonial.update');
// Route::delete('/backoffice/testimonials/destroy/{id}', 'TestimonialController@destroy')->name('testimonial.delete');

Route::get('/backoffice/products', 'StaffController@index');
Route::get('/backoffice/products/create', 'StaffController@create');
Route::post('/backoffice/products/create', 'StaffController@store')->name('make.staff');
Route::get('/backoffice/products/{id}', 'StaffController@show');
Route::get('/backoffice/products/edit/{id}', 'StaffController@edit');
Route::post('/backoffice/products/edit/{id}', 'StaffController@update')->name('update.staff');
Route::delete('/backoffice/products/destroy/{id}', 'StaffController@destroy')->name('delete.staff');

Route::get('/backoffice/page-setting', 'PageSettingController@index');
Route::post('/backoffice/page-setting/edit/{id}', 'PageSettingController@update')->name('page-setting.update');