<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;
use App\Language;

class LanguageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $lang = new Language();
      $lang->selected_language = '1';
      $lang->save();
    }
}
