<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;
use App\Menu;

class MenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $menu = new Menu();
       $menu->label = 'English';
       $menu->url = '/';
       $menu->order = '1';
       $menu->save();
    }
}
