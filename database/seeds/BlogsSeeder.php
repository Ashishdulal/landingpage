<?php

use Illuminate\Support\Facades\DB;
use App\Blog;
use Illuminate\Database\Seeder;

class BlogsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $bcat = new Blog();
        $bcat->cat_id = '1';
        $bcat->title = 'Test Blog';
        $bcat->description = 'lorem Ipsum Get imum';
        $bcat->f_image = 'default-thumbnail.png';
        $bcat->i_image = 'default-thumbnail.png';
        $bcat->save();
    }
}
