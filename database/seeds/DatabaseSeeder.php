<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(StaffSeeder::class);
        $this->call(SliderSeeder::class);
        $this->call(ServicesSeeder::class);
        $this->call(BlogsSeeder::class);
        $this->call(LanguageSeeder::class);
        $this->call(MenuSeeder::class);
        $this->call(BlogCategoriesSeeder::class);
        $this->call(LandingPageSeeder::class);
        $this->call(PageSetttingsSeeder::class);
    }
}
