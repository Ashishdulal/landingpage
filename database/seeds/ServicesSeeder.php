<?php

use Illuminate\Database\Seeder;
use App\Service;
use Illuminate\Support\Facades\DB;

class ServicesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $service = new Service();
        $service->name ='Demo Service';
        $service->image ='default-thumbnail.png';
        $service->description ='Hello World';
        $service->language_id ='1';
        $service->save();

        $service = new Service();
        $service->name ='Demo Quality';
        $service->image ='default-thumbnail.png';
        $service->description ='Hello World';
        $service->language_id ='1';
        $service->save();

        $service = new Service();
        $service->name ='Demo Assurance';
        $service->image ='default-thumbnail.png';
        $service->description ='Hello World';
        $service->language_id ='1';
        $service->save();
    }
}
