<?php

use App\landingPage;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LandingPageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $page = new landingPage();
        $page->service_details = '<h2>Our Services</h2><p><img src="/images/line.png" /></p><p>We offer best professional online solution services with combination of affordability and quality.</p>';
        $page->team_details = '<h2>Our Products</h2><h2><img src="/images/line.png" /></h2><p>Our team have years of experience creating custom mobile applications for iOS and Android and Web.</p>';
        $page->signup_details = '<h2>Signup for Free</h2><p><img src="/images/line.png" /></p><p><strong>We&#39;ll follow up</strong></p><p>One of our workspace experts will reach out to you based on your communication preferences.</p>';
        $page->signup_image1 = 'default-thumbnail.png';
        $page->signup_image2 = 'default-thumbnail.png';
        $page->video_text = 'Test Text';
        $page->lower_video = 'default-video.mp4';
        $page->otp_video = 'Test Text';
        $page->otp_text = 'Test Text';
        $page->text1 = 'Test Text';
        $page->text2 = 'Test Text';
        $page->text3 = 'Test Text';
        $page->text4 = 'Test Text';
        $page->language_id = '1';
        $page->save();
    }
}