<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\staff;

class StaffSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $staff = new staff();
        $staff->name = 'lorem Ipsum';
        $staff->image = 'default-thumbnail.png';
        $staff->description = 'Lorem Ipsum Summet Whats oal.';
        $staff->language_id = '1';
        $staff->save();
        $staff = new staff();
        $staff->name = 'Summet Ipsum';
        $staff->image = 'default-thumbnail.png';
        $staff->description = 'Lorem Ipsum Summet Whats oal.';
        $staff->language_id = '1';
        $staff->save();
        $staff = new staff();
        $staff->name = 'Oata Ipsum';
        $staff->image = 'default-thumbnail.png';
        $staff->description = 'Lorem Ipsum Summet Whats oal.';
        $staff->language_id = '1';
        $staff->save();
    }
}
