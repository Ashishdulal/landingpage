<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;
use App\Blogcategory;

class BlogCategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $bcat = new Blogcategory();
        $bcat->title = 'Newsletter';
        $bcat->save();
    }
}
