<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Slider;

class SliderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $slider = new Slider();
        $slider->name = 'Demo Slider';
        $slider->image ='default-slider.png';
        $slider->title = 'Hello Demo World';
        $slider->title_1 = 'Hello World';
        $slider->description = 'Hello to you all.';
        $slider->language_id = '1';
        $slider->save();
    }
}
