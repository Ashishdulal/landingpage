<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLandingPagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('landing_pages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('service_details')->nullable();
            $table->text('team_details')->nullable();
            $table->text('signup_details')->nullable();
            $table->text('signup_image1')->nullable();
            $table->text('signup_image2')->nullable();
            $table->text('video_text')->nullable();
            $table->text('lower_video')->nullable();
            $table->text('otp_video')->nullable();
            $table->text('otp_text')->nullable();
            $table->text('text1')->nullable();
            $table->text('text2')->nullable();
            $table->text('text3')->nullable();
            $table->text('text4')->nullable();
            $table->integer('language_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('landing_pages');
    }
}
