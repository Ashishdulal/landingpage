-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 17, 2020 at 11:01 PM
-- Server version: 5.6.41-84.1
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mysedap_landing`
--

-- --------------------------------------------------------

--
-- Table structure for table `blogcategories`
--

CREATE TABLE `blogcategories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `language_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blogcategories`
--

INSERT INTO `blogcategories` (`id`, `title`, `language_id`, `created_at`, `updated_at`) VALUES
(1, 'Newsletter', 4, '2020-01-21 15:30:05', '2020-02-09 23:03:23');

-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--

CREATE TABLE `blogs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cat_id` int(11) NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `f_image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `i_image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `language_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blogs`
--

INSERT INTO `blogs` (`id`, `cat_id`, `title`, `description`, `f_image`, `i_image`, `language_id`, `created_at`, `updated_at`) VALUES
(1, 1, 'WE HELP YOU GET MORE TRAFFIC', '<h3><strong>Lorem ipsum dolor sit amet,</strong></h3>\r\n\r\n<p>consectetur adipiscing elit. Vivamus dignissim sem eu diam iaculis sodales. Vestibulum vitae ultricies dolor. Nam leo dui, lacinia quis leo quis, dignissim ultricies velit. Integer lobortis erat enim, nec commodo risus malesuada et. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Cras ac mi vitae lectus eleifend dictum nec sit amet metus. Donec id nunc porta, consequat nisi ac, imperdiet justo. Duis tincidunt commodo urna, vel venenatis ex euismod non. Pellentesque mollis, mi sit amet volutpat euismod, neque magna pulvinar ipsum, et auctor diam libero sed enim. Integer non molestie mauris. Praesent faucibus malesuada ipsum id pharetra.</p>\r\n\r\n<h3><strong>Morbi in fringilla orci.</strong></h3>\r\n\r\n<p>Duis ac urna sit amet nisl pretium ullamcorper vitae ac urna. Curabitur ultricies lectus ipsum, at malesuada felis sollicitudin vitae. Suspendisse nibh massa, aliquam eget commodo nec, aliquet in felis. Pellentesque vitae mi vitae sapien venenatis finibus eget porta nunc. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Ut in lorem vitae mi aliquet aliquam. Aliquam ac molestie ipsum, eget accumsan augue. Morbi ex lorem, dictum sed faucibus at, ullamcorper at enim. Mauris elementum tortor ut mi tincidunt commodo. Duis euismod, ex vel lobortis mollis, lectus leo faucibus massa, vel volutpat purus mauris a sapien.</p>\r\n\r\n<p>Curabitur varius dui ac lectus tempor, at elementum nulla scelerisque. Cras tincidunt sem risus, a tincidunt libero venenatis et. Fusce facilisis bibendum blandit. Vestibulum suscipit metus at odio aliquet, sed vehicula augue interdum. Pellentesque a ullamcorper magna. Pellentesque interdum ligula libero, eget tincidunt velit cursus ac. Maecenas congue euismod turpis, quis viverra purus porta ut. Maecenas molestie cursus libero, in venenatis lacus consequat id. Cras erat lacus, pharetra ac arcu id, viverra tempus leo. Ut sed auctor ex. Aliquam finibus fringilla turpis eu consectetur. Maecenas in dignissim nisi, eu aliquet justo. Aenean iaculis finibus congue.</p>\r\n\r\n<h3><strong>Sed imperdiet velit purus.</strong></h3>\r\n\r\n<p>Sed consequat bibendum turpis, id rhoncus odio sagittis at. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Sed ac tincidunt nulla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed scelerisque vel eros id elementum. Vivamus volutpat urna ex, quis posuere nunc faucibus a. Phasellus eget pellentesque ex. Aliquam ligula tortor, efficitur at mi sed, consequat varius arcu. Pellentesque congue mattis vestibulum. Praesent quis mattis sapien, non dapibus diam. Sed porta magna pretium est pretium, at molestie enim varius. Sed nec hendrerit enim. Curabitur efficitur orci eget purus facilisis dictum. Cras et lacus quam.</p>\r\n\r\n<h3><strong>Praesent bibendum dolor sit amet varius scelerisque.</strong></h3>\r\n\r\n<p>Suspendisse vitae porttitor erat, et fermentum justo. Proin interdum dui fermentum maximus luctus. Donec vel tortor et quam dignissim sodales at a orci. Maecenas lacinia gravida placerat. Nunc dapibus massa ac augue ornare convallis. In non purus euismod, eleifend libero in, ultricies diam. Nulla faucibus efficitur sem volutpat ornare. Quisque malesuada erat quis tempor interdum. Sed non nunc nec nunc aliquam posuere vitae id arcu. Aenean pellentesque velit vitae scelerisque imperdiet. Nam nibh leo, lacinia non aliquam at, vehicula non tortor. Sed nunc purus, hendrerit vitae arcu in, viverra euismod ligula. Praesent volutpat enim ac quam finibus, at elementum ipsum aliquet. Suspendisse ultricies justo ut velit maximus, eu tempus elit maximus. Ut sed lacinia enim.</p>', 'blog1581311964.services1561439116.png', 'blog1579599064.services1561439116.png', 5, '2020-01-21 15:31:04', '2020-02-09 23:34:24'),
(2, 1, 'Voluptate tempor ill', '<p>dasascsacasca</p>', 'default-thumbnail.png', 'default-thumbnail.png', 2, '2020-02-09 23:43:02', '2020-02-09 23:43:02');

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `jobs`
--

INSERT INTO `jobs` (`id`, `name`, `image`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Sean Malone', 'default-thumbnail.png', 'Hic corporis aut lau', '2020-01-20 03:11:59', '2020-01-20 03:11:59'),
(2, 'Tana Craig', 'default-thumbnail.png', '<p>asdfgh</p>\r\n\r\n<p>asfdfg</p>', '2020-01-20 03:20:58', '2020-01-20 03:20:58');

-- --------------------------------------------------------

--
-- Table structure for table `landing_pages`
--

CREATE TABLE `landing_pages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `service_details` text COLLATE utf8mb4_unicode_ci,
  `team_details` text COLLATE utf8mb4_unicode_ci,
  `signup_details` text COLLATE utf8mb4_unicode_ci,
  `signup_image1` text COLLATE utf8mb4_unicode_ci,
  `signup_image2` text COLLATE utf8mb4_unicode_ci,
  `video_text` text COLLATE utf8mb4_unicode_ci,
  `lower_video` text COLLATE utf8mb4_unicode_ci,
  `otp_video` text COLLATE utf8mb4_unicode_ci,
  `otp_text` text COLLATE utf8mb4_unicode_ci,
  `text1` text COLLATE utf8mb4_unicode_ci,
  `text2` text COLLATE utf8mb4_unicode_ci,
  `text3` text COLLATE utf8mb4_unicode_ci,
  `text4` text COLLATE utf8mb4_unicode_ci,
  `language_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `landing_pages`
--

INSERT INTO `landing_pages` (`id`, `service_details`, `team_details`, `signup_details`, `signup_image1`, `signup_image2`, `video_text`, `lower_video`, `otp_video`, `otp_text`, `text1`, `text2`, `text3`, `text4`, `language_id`, `created_at`, `updated_at`) VALUES
(1, '<h2>Our Services</h2>\r\n\r\n<p><img src=\"/images/line.png\" /></p>\r\n\r\n<p>We offer best professional online solution services with combination of affordability and quality.</p>', '<h2>Our Products</h2>\r\n\r\n<h2><img src=\"/images/line.png\" style=\"font-size:13px\" /></h2>\r\n\r\n<p>Our team have years of experience creating custom mobile applications for iOS and Android, enterprise level platform, corporate websites, IT solutions, Graphic Designs and more.</p>', '<h2>Signup for Free</h2>\r\n\r\n<p><img src=\"/images/line.png\" /></p>\r\n\r\n<p><strong>We&#39;ll follow up</strong></p>\r\n\r\n<p>One of our workspace experts will reach out to you based on your communication preferences.</p>', 'signup_image11581334191.signup_image11579592528.services1577251748.services1569411621.services1561439166.jpg', 'signup_image21581334191.signup_image11579591604.services1569409932.services1561439205.jpg', '<h1><samp>Toronto&rsquo;s Search Engine Experts In Malaysia</samp></h1>\r\n\r\n<p>&nbsp;</p>', 'default-video.mp4', 'Test Text', 'Test Text', 'class=\"icon fa-whatsapp chat-logo\" target=\"_blank\" href=\"https://wa.me/15551234567\" ]', '1Wk9jZtAGRP7XluqZidTRd3bXti', '3Lx4jWQeBiXoZ9Qyn0JTTVPR5Lfp23fsyvF2JQoF', 'Test Text', 1, '2020-02-09 05:47:07', '2020-02-17 17:06:19'),
(2, '<h2>perkhidmatan kami</h2>\r\n\r\n<p><img src=\"/images/line.png\" /></p>\r\n\r\n<p>Kami menawarkan perkhidmatan penyelesaian dalam talian profesional terbaik dengan kombinasi kemampuan dan kualiti.</p>', '<h2>Pasukan Profesional kami</h2>\r\n\r\n<p><img src=\"/images/line.png\" /></p>\r\n\r\n<p>Our team have years of experience creating custom mobile applications for iOS and Android, enterprise level platform, corporate websites, IT solutions, Graphic Designs and more.</p>', '<h2>Hubungi Kami</h2>\r\n\r\n<h2><img src=\"http://127.0.0.1:8000/images/line.png\" /></h2>\r\n\r\n<p><strong>Kami akan menindaklanjuti</strong></p>\r\n\r\n<p>Salah satu daripada pakar kerja kami akan menghubungi anda berdasarkan keutamaan komunikasi anda.<br />\r\n&nbsp;</p>', 'signup_image11581334191.signup_image11579592528.services1577251748.services1569411621.services1561439166.jpg', 'signup_image21581334191.signup_image11579591604.services1569409932.services1561439205.jpg', '<h1><samp>Pakar Search Engine Toronto Di Malaysi</samp></h1>\r\n\r\n<p>&nbsp;</p>', 'default-video.mp4', 'Test Text', 'Test Text', 'Test Text', '1Wk9jZtAGRP7XluqZidTRd3bXti', '3Lx4jWQeBiXoZ9Qyn0JTTVPR5Lfp23fsyvF2JQoF', 'Test Text', 2, '2020-02-09 05:47:07', '2020-02-11 03:46:08'),
(3, '<h2>हाम्रो सेवाहरू</h2>\r\n\r\n<p><img src=\"/images/line.png\" /></p>\r\n\r\n<p>हामी सबै भन्दा राम्रो व्यावसायिक अनलाइन समाधान सेवाहरू अफोर्डेबिलिटी र गुणवत्ताको संयोजनको साथ प्रस्ताव गर्दछौं।</p>', '<h2>हाम्रो पेशेवर टीम</h2>\r\n\r\n<p><img src=\"/images/line.png\" /></p>\r\n\r\n<p>हाम्रो टोलीसँग आईओएस र एन्ड्रोइड, उद्यम स्तर प्लेटफर्म, कर्पोरेट वेबसाइटहरू, आईटी समाधान, ग्राफिक डिजाइन र अधिकको लागि अनुकूलित मोबाइल अनुप्रयोगहरू सिर्जना गर्न वर्षौंको अनुभव छ।</p>', '<h2>हामीलाई सम्पर्क गर्नुहोस</h2>\r\n\r\n<p><img src=\"/images/line.png\" /></p>\r\n\r\n<p><strong>हामी अनुगमन गर्नेछौं</strong></p>\r\n\r\n<p>हाम्रो वर्कस्पेस विशेषज्ञहरू मध्ये एक तपाईंको सम्पर्क प्राथमिकतामा आधारित तपाईं समक्ष पुग्नेछ।</p>', 'signup_image11581334191.signup_image11579592528.services1577251748.services1569411621.services1561439166.jpg', 'signup_image21581334191.signup_image11579591604.services1569409932.services1561439205.jpg', '<h1><samp>मलेशियामा टोरन्टोको खोज इञ्जिन विशेषज्ञहर</samp></h1>\r\n\r\n<p>&nbsp;</p>', 'default-video.mp4', 'Test Text', 'Test Text', 'Test Text', '1Xt4MtnCh63OHHTWwWcGPbsUJDs', 'oGnkYahU9arriomm8Xbh4Mcem4uzYYTSp7afTC1S', 'Test Text', 3, '2020-02-09 05:47:07', '2020-02-16 17:28:26'),
(4, '<h2>pelayanan kami</h2>\r\n\r\n<p><img src=\"/images/line.png\" /></p>\r\n\r\n<p>Kami menawarkan layanan solusi online profesional terbaik dengan kombinasi keterjangkauan dan kualitas.</p>', '<h2>Tim Profesional kami</h2>\r\n\r\n<p><img src=\"/images/line.png\" /></p>\r\n\r\n<p>Tim kami memiliki pengalaman bertahun-tahun dalam menciptakan aplikasi seluler khusus untuk iOS dan Android, platform tingkat perusahaan, situs web perusahaan, solusi IT, Desain Grafis dan banyak lagi.</p>', '<h2>Hubungi kami</h2>\r\n\r\n<p><img src=\"/images/line.png\" /></p>\r\n\r\n<p><strong>Kami akan menindaklanjutinya</strong><br />\r\nSalah satu pakar ruang kerja kami akan menghubungi Anda berdasarkan preferensi komunikasi Anda.</p>', 'signup_image11581334191.signup_image11579592528.services1577251748.services1569411621.services1561439166.jpg', 'signup_image21581334191.signup_image11579591604.services1569409932.services1561439205.jpg', '<h1><samp>Ahli Mesin Pencari Toronto Di Malaysi</samp></h1>\r\n\r\n<p>&nbsp;</p>', 'default-video.mp4', 'Test Text', 'Test Text', '[ class=\"icon fa-whatsapp chat-logo\" target=\"_blank\" href=\"https://wa.me/15551234567\" ]', '1Wk9jZtAGRP7XluqZidTRd3bXti', '3Lx4jWQeBiXoZ9Qyn0JTTVPR5Lfp23fsyvF2JQoF', 'Test Text', 4, '2020-02-09 05:47:07', '2020-02-18 10:57:36'),
(5, '<pre>\r\n私たちのサービス</pre>\r\n\r\n<p><img src=\"/images/line.png\" /></p>\r\n\r\n<p>手頃な価格と品質の組み合わせで最高のプロフェッショナルなオンラインソリューションサービスを提供します。</p>', '<pre>\r\n私たちの専門チーム</pre>\r\n\r\n<p><img src=\"/images/line.png\" /></p>\r\n\r\n<p>私たちのチームは、iOSおよびアンドロイド向けのカスタムモバイルアプリケーション、エンタープライズレベルのプラットフォーム、企業Webサイト、ITソリューション、グラフィックデザインなどを長年にわたって作成してきました。</p>', '<pre>\r\nお問い合わせ</pre>\r\n\r\n<p><img src=\"/images/line.png\" /></p>\r\n\r\n<p>フォローアップします</p>\r\n\r\n<p>ワークスペースのエキスパートの1人が、コミュニケーションの好みに基づいてあなたに連絡します。</p>', 'signup_image11581334191.signup_image11579592528.services1577251748.services1569411621.services1561439166.jpg', 'signup_image21581334191.signup_image11579591604.services1569409932.services1561439205.jpg', '<h1>トロントのマレーシアの検索エンジンエキスパート</h1>', 'default-video.mp4', 'Test Text', 'Test Text', 'class=\"icon fa-whatsapp chat-logo\" target=\"_blank\" href=\"https://wa.me/15551234567\" ]', '1Wk9jZtAGRP7XluqZidTRd3bXti', '3Lx4jWQeBiXoZ9Qyn0JTTVPR5Lfp23fsyvF2JQoF', 'Test Text', 5, '2020-02-09 05:47:07', '2020-02-11 03:40:56'),
(6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1Wk9jZtAGRP7XluqZidTRd3bXti', NULL, NULL, 7, '2020-02-16 14:55:15', '2020-02-16 14:55:15');

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE `languages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `selected_language` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `language_text` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`id`, `selected_language`, `language_text`, `created_at`, `updated_at`) VALUES
(1, '4', NULL, '2020-02-18 10:57:02', '2020-02-18 10:57:02');

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `label` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `language_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `label`, `url`, `order`, `parent_id`, `language_id`, `created_at`, `updated_at`) VALUES
(1, 'English', '/', 1, NULL, NULL, '2020-02-07 17:12:10', '2020-02-16 11:59:48'),
(2, 'Malay', '/', 2, 1, NULL, '2020-02-07 17:12:33', '2020-02-14 13:32:32'),
(3, 'Nepali', '/', 3, 1, NULL, '2020-02-07 17:13:34', '2020-02-07 17:13:34'),
(4, 'Indonesian', '/', 4, 1, NULL, '2020-02-09 22:31:03', '2020-02-09 22:31:27'),
(5, 'Japanese', '/', 5, 1, NULL, '2020-02-09 22:31:44', '2020-02-09 22:31:44'),
(8, 'demo', '/', 7, 1, NULL, '2020-02-16 16:07:28', '2020-02-16 16:07:28');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_07_09_070457_create_services_table', 1),
(5, '2019_07_09_074934_create_blogcategories_table', 1),
(6, '2019_07_09_075601_create_blogs_table', 1),
(7, '2019_07_09_082710_create_sliders_table', 1),
(9, '2019_08_01_094700_create_staff_table', 1),
(11, '2019_08_30_083337_create_experiences_table', 1),
(12, '2019_09_26_114358_create_jobs_table', 1),
(14, '2019_11_13_090226_create_menus_table', 1),
(16, '2019_12_25_105440_add_image_to_users_table', 1),
(20, '2019_12_30_054532_create_page_settings_table', 1),
(21, '2020_01_21_054256_create_landing_pages_table', 1),
(24, '2020_02_10_053939_create_languages_table', 2),
(25, '2019_11_24_112858_create_newsletters_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `newsletters`
--

CREATE TABLE `newsletters` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `first_name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `language_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `newsletters`
--

INSERT INTO `newsletters` (`id`, `first_name`, `last_name`, `phone_number`, `email`, `language_id`, `created_at`, `updated_at`) VALUES
(1, 'Shana', 'Morin', '+1 (618) 697-2683', 'sunyhem@mailinator.net', NULL, '2020-02-10 03:40:17', '2020-02-10 03:40:17'),
(12, 'aaa', 'aaa', '23456789', 'aaa@aaa.com', NULL, '2020-02-14 10:34:49', '2020-02-14 10:34:49'),
(13, 'Ronan', 'Walton', '08-585-2464', 'xogowef@mailinator.net', NULL, '2020-02-14 14:32:53', '2020-02-14 14:32:53');

-- --------------------------------------------------------

--
-- Table structure for table `page_settings`
--

CREATE TABLE `page_settings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `site_title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `tagline` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `site_url` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `site_logo` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `site_favicon` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `footer_text` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `footer_visibility` tinyint(1) NOT NULL DEFAULT '0',
  `copyright_text` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `permalink_seo` text COLLATE utf8mb4_unicode_ci,
  `meta_keywords_seo` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description_seo` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `page_settings`
--

INSERT INTO `page_settings` (`id`, `site_title`, `tagline`, `site_url`, `email_address`, `site_logo`, `site_favicon`, `footer_text`, `footer_visibility`, `copyright_text`, `permalink_seo`, `meta_keywords_seo`, `meta_description_seo`, `created_at`, `updated_at`) VALUES
(1, 'Demo', 'lorem ipsum', 'demosite.com', 'demosite@mail.com', 'home1581933719.homepage_logo.png', 'favicon1581665761.title-logo.png', '<p>Copyright&copy; 2019 All Rights Reserved by Test Tech</p>', 0, '<p>Copyright&copy; 2019 All Rights Reserved by Test Tech</p>', 'demosite', 'demosite', 'demosite', '2020-02-09 05:47:07', '2020-02-17 16:01:59');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('admin@admin.com', '$2y$10$4bCAIXpm1ZCVwuF7T.NYFuYIcLOwSuY26CPIpre/T/ovNmalnkCYC', '2020-02-13 11:00:09'),
('azizdulal.ad@gmail.com', '$2y$10$rKst.Z.dyBpKj7R/y2gmvuZ91oexWafWbmfni52FrJPLilpD6pkam', '2020-02-13 11:39:04');

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `language_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `name`, `image`, `description`, `language_id`, `created_at`, `updated_at`) VALUES
(1, 'Less Work For You', 'services1581396076.services1579513326.world-wide-web.png', '<p>We Integrate with your payroll and handle all the reporting, compilance, and investments.</p>', 1, '2020-01-16 02:36:26', '2020-02-09 22:51:24'),
(2, 'Less Work For You', 'services1581396120.services1579513343.ecology.png', '<p>We Integrate with your payroll and handle all the reporting, compilance, and investments.</p>', 1, '2020-01-20 03:55:09', '2020-02-09 22:57:57'),
(3, 'Less Work For You', 'services1581396247.services1579513367.business-and-finance.png', '<p>We Integrate with your payroll and handle all the reporting, compilance, and investments.</p>', 1, '2020-01-20 03:57:47', '2020-02-09 22:58:02'),
(4, 'तपाईंको लागि कम काम', 'services1581397651.services1579513326.world-wide-web.png', '<p>हामी KX टेक्नोलोजीले&nbsp; तालिकामा असाधारण प्रतिभा ल्याउँदछन् र हाम्रो प्लेटफर्मको निरन्तर वृद्धिको लागि महत्वपूर्ण भूमिका खेलेका छन्।</p>', 3, '2020-02-10 01:49:43', '2020-02-11 11:09:11'),
(5, 'Pembangunan Aplikasi Mudah Alih', 'services1581396076.services1579513326.world-wide-web.png', '<p>Mempunyai aplikasi untuk promosi perniagaan telah ditentukan untuk menjadi alat pemasaran yang paling berkesan kerana ia mendorong traffik dan hasil ke laman web secara massal.</p>', 2, '2020-02-11 10:37:50', '2020-02-11 10:45:10'),
(6, 'Reka bentuk grafik', 'services1581396120.services1579513343.ecology.png', '<p>Dari reka bentuk logo, strategi penjenamaan semula dan bahan pemasaran, kami menawarkan pelbagai perkhidmatan penjenamaan dan reka bentuk grafik</p>', 2, '2020-02-11 10:38:44', '2020-02-11 10:44:22'),
(7, 'Reka Bentuk Laman Web', 'services1581396247.services1579513367.business-and-finance.png', '<p>Untuk membantu perniagaan anda menarik lebih banyak pelawat dan menyimpannya di laman web anda, kami menawarkan perkhidmatan reka bentuk web yang profesional dan berpatutan.</p>', 2, '2020-02-11 10:39:56', '2020-02-11 10:44:07'),
(8, 'तपाईंको लागि कम खर्च', 'services1581397847.services1579513343.ecology.png', '<p>हामी KX टेक्नोलोजीले &nbsp;तालिकामा असाधारण प्रतिभा ल्याउँदछन् र हाम्रो प्लेटफर्मको निरन्तर वृद्धिको लागि महत्वपूर्ण भूमिका खेलेका छन्।</p>', 3, '2020-02-11 11:10:47', '2020-02-11 11:10:47'),
(9, 'तपाईंको लागि उच्च दक्षता', 'services1581397889.services1579513367.business-and-finance.png', '<p>हामी KX टेक्नोलोजीले &nbsp;तालिकामा असाधारण प्रतिभा ल्याउँदछन् र हाम्रो प्लेटफर्मको निरन्तर वृद्धिको लागि महत्वपूर्ण भूमिका खेलेका छन्।</p>', 3, '2020-02-11 11:11:29', '2020-02-11 11:11:29'),
(10, 'Kurang Bekerja Untuk Anda', 'services1581398183.services1579513326.world-wide-web.png', '<p>Kami Mengintegrasikan dengan daftar gaji Anda dan menangani semua pelaporan, kompilasi, dan investasi.</p>', 4, '2020-02-11 11:16:23', '2020-02-11 11:16:23'),
(11, 'Kurang Upaya Untuk Anda', 'services1581398236.services1579513343.ecology.png', '<p>Kami Mengintegrasikan dengan daftar gaji Anda dan menangani semua pelaporan, kompilasi, dan investasi.</p>', 4, '2020-02-11 11:17:16', '2020-02-11 11:17:16'),
(12, 'Efisiensi lebih tinggi untuk Anda', 'services1581398294.services1579513367.business-and-finance.png', '<p>Kami Mengintegrasikan dengan daftar gaji Anda dan menangani semua pelaporan, kompilasi, dan investasi.</p>', 4, '2020-02-11 11:18:14', '2020-02-11 11:18:14'),
(13, 'あなたのために少ない仕事', 'services1581398656.services1579513326.world-wide-web.png', '<p>給与計算と統合し、すべてのレポート、コンプライアンス、および投資を処理します。</p>', 5, '2020-02-11 11:24:16', '2020-02-11 11:24:16'),
(14, 'あなたのための少ない労力', 'services1581398697.services1579513343.ecology.png', '<p>給与計算と統合し、すべてのレポート、コンプライアンス、および投資を処理します。</p>', 5, '2020-02-11 11:24:57', '2020-02-11 11:24:57'),
(15, 'あなたのためのより高い効率', 'services1581398746.services1579513367.business-and-finance.png', '<p>給与計算と統合し、すべてのレポート、コンプライアンス、および投資を処理します。</p>', 5, '2020-02-11 11:25:46', '2020-02-11 11:25:46');

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

CREATE TABLE `sliders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci,
  `title_1` text COLLATE utf8mb4_unicode_ci,
  `description` text COLLATE utf8mb4_unicode_ci,
  `language_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sliders`
--

INSERT INTO `sliders` (`id`, `name`, `image`, `title`, `title_1`, `description`, `language_id`, `created_at`, `updated_at`) VALUES
(1, 'Kevin Greene', 'Slider1581398592.Slider1569408219.Slider1558599745.png', 'WE HELP YOU GET MORE TRAFFIC,', 'LEADS & SALES', '<h1><samp>Toronto&rsquo;s Search Engine Experts&nbsp;</samp><samp>In Malaysia.</samp></h1>', 1, '2020-01-16 02:40:02', '2020-02-12 11:38:09'),
(3, 'स्लाइडर', 'Slider1581398592.Slider1569408219.Slider1558599745.png', 'हामी तपाईंलाई अधिक पहुँच प्राप्त गर्न मद्दत गर्दछौं,', 'नेतृत्व र बिक्री', '<p>मलेशियामा टोरन्टोको खोज इञ्जिन विशेषज्ञहरू।</p>', 3, '2020-02-09 22:25:46', '2020-02-16 17:06:34'),
(4, 'slider1', 'Slider1581398592.Slider1569408219.Slider1558599745.png', 'より多くのトラフィックを獲得できるようお手伝いします。', 'リード＆セールス', '<h1>トロントのマレーシアの検索エンジンエキスパート。</h1>', 5, '2020-02-09 22:35:37', '2020-02-11 11:23:12'),
(5, 'slider', 'Slider1581398592.Slider1569408219.Slider1558599745.png', 'KAMI MEMBANTU ANDA MENDAPATKAN TRAFIK,', 'LEADS & SALES', '<h1>Pakar Search Engine Toronto Di Malaysia.</h1>', 2, '2020-02-10 02:43:49', '2020-02-12 11:42:17'),
(6, 'one slider', 'Slider1581398027.Slider1569408219.Slider1558599745.png', 'KAMI MEMBANTU ANDA MENDAPAT LEBIH BANYAK LALU LINTAS,', 'LEAD & PENJUALAN', '<h1>Ahli Mesin Pencari Toronto Di Malaysia.</h1>', 4, '2020-02-11 11:13:47', '2020-02-11 11:14:53');

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

CREATE TABLE `staff` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `language_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `staff`
--

INSERT INTO `staff` (`id`, `name`, `image`, `description`, `language_id`, `created_at`, `updated_at`) VALUES
(1, 'Octavia Sanchez', 'staffs1581665461.blog1569412266.person_1.jpg', '<p>.kndcsld</p>', 1, '2020-01-16 02:38:24', '2020-02-14 13:31:01'),
(2, 'Galena Hardy', 'staffs1581665479.blog1569412293.person_7.jpg', '<p>.kdjcds;cvdsb;ksdbvds</p>', 1, '2020-01-16 02:38:35', '2020-02-14 13:31:19'),
(3, 'Jermaine Larsen', 'staffs1581665493.blog1569412266.nature_small_9.jpg', '<p>lkvvrlf</p>', 1, '2020-01-16 02:38:50', '2020-02-14 13:31:33'),
(4, 'Odette Greene', 'staffs1581665511.staffs1569412418.person_3.jpg', '<p>wlkdnlknclknd</p>', 1, '2020-01-16 03:15:31', '2020-02-14 13:31:51'),
(5, 'आशिष दुलाल', 'staffs1581397219.blog1569412293.person_7.jpg', '<p>नेपालले क्रिकेट विश्वकप लिग २ मा पहिलो विजय हात पारेको छ । नेपालले शनिबार अमेरिकालाई ३५ रनले पराजित गर्दै घरेलु मैदानमा एकदिवसीय अन्तर्राष्ट्रियमा पहिलो विजय निकालेको हो । नेपालले बुधबार मात्र घरेलु मैदानमा पहिलो एकदिवसीय अन्तर्राष्ट्रिय खेलेको थियो तर ओमानसँगको त्यस खेलमा विजय निकाल्न सकेको थिएन ।</p>', 3, '2020-02-10 03:06:52', '2020-02-11 11:00:19'),
(6, 'Octavia Sanchez', 'staffs1581395212.signup_image11579592515.blog1569412266.nature_small_9.jpg', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, but do not use temporarily for the labour and dolore magna aliqua. Ini adalah cara yang paling mudah untuk menjalankan kerja-kerja latihan yang dilakukan oleh pelanggan untuk mendapatkan komitmen. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Sekali lagi, kami tidak dapat memberi peluang kepada mereka untuk bekerja di tempat kerja mereka.</p>', 2, '2020-02-11 10:26:52', '2020-02-11 10:33:44'),
(7, 'Aquila Branch', 'staffs1581395680.blog1569412266.person_1.jpg', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, but do not use temporarily for the labour and dolore magna aliqua. Ini adalah cara yang paling mudah untuk menjalankan kerja-kerja latihan yang dilakukan oleh pelanggan untuk mendapatkan komitmen. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Sekali lagi, kami tidak dapat memberi peluang kepada mereka untuk bekerja di tempat kerja mereka.</p>', 2, '2020-02-11 10:34:40', '2020-02-11 10:34:40'),
(8, 'Teagan Marsh', 'staffs1581395707.blog1569412293.person_7.jpg', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, but do not use temporarily for the labour and dolore magna aliqua. Ini adalah cara yang paling mudah untuk menjalankan kerja-kerja latihan yang dilakukan oleh pelanggan untuk mendapatkan komitmen. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Sekali lagi, kami tidak dapat memberi peluang kepada mereka untuk bekerja di tempat kerja mereka.</p>', 2, '2020-02-11 10:35:07', '2020-02-11 10:35:07'),
(9, 'सुसान ब्यारी', 'staffs1581397203.blog1569412266.person_1.jpg', '<p>नेपालले क्रिकेट विश्वकप लिग २ मा पहिलो विजय हात पारेको छ । नेपालले शनिबार अमेरिकालाई ३५ रनले पराजित गर्दै घरेलु मैदानमा एकदिवसीय अन्तर्राष्ट्रियमा पहिलो विजय निकालेको हो । नेपालले बुधबार मात्र घरेलु मैदानमा पहिलो एकदिवसीय अन्तर्राष्ट्रिय खेलेको थियो तर ओमानसँगको त्यस खेलमा विजय निकाल्न सकेको थिएन ।</p>', 3, '2020-02-11 11:00:03', '2020-02-11 11:00:03'),
(10, 'गेराल्डिन आर्मस्ट्र', 'staffs1581397312.blog1569412266.nature_small_9.jpg', '<p>नेपालले क्रिकेट विश्वकप लिग २ मा पहिलो विजय हात पारेको छ । नेपालले शनिबार अमेरिकालाई ३५ रनले पराजित गर्दै घरेलु मैदानमा एकदिवसीय अन्तर्राष्ट्रियमा पहिलो विजय निकालेको हो । नेपालले बुधबार मात्र घरेलु मैदानमा पहिलो एकदिवसीय अन्तर्राष्ट्रिय खेलेको थियो तर ओमानसँगको त्यस खेलमा विजय निकाल्न सकेको थिएन ।</p>', 3, '2020-02-11 11:01:52', '2020-02-11 11:01:52'),
(11, 'Kiayada Hines', 'staffs1581398436.blog1569412266.person_1.jpg', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', 4, '2020-02-11 11:19:45', '2020-02-11 11:20:36'),
(12, 'Lacey Townsend', 'staffs1581398421.blog1569412293.person_7.jpg', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', 4, '2020-02-11 11:20:21', '2020-02-11 11:20:21'),
(13, 'Hannah Hale', 'staffs1581398466.blog1569412266.nature_small_9.jpg', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', 4, '2020-02-11 11:21:06', '2020-02-11 11:21:06'),
(14, 'アシッシュデュラル', 'staffs1581398885.blog1569412266.person_1.jpg', '<p>花よりだんご&nbsp;</p>', 5, '2020-02-11 11:28:05', '2020-02-11 11:28:05'),
(15, 'リテン・ダハル', 'staffs1581398969.blog1569412266.nature_small_9.jpg', '<p>花よりだんご&nbsp;</p>', 5, '2020-02-11 11:29:29', '2020-02-11 11:29:29'),
(16, 'ナクル・ブダトキ', 'staffs1581399035.blog1569412293.person_7.jpg', '<p>花よりだんご&nbsp;</p>', 5, '2020-02-11 11:30:35', '2020-02-11 11:30:35'),
(17, 'Arthur England', 'default-thumbnail.png', '<p>sjdcgsdjcgds</p>', 1, '2020-02-17 17:24:35', '2020-02-17 17:24:35');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `image` text COLLATE utf8mb4_unicode_ci,
  `role` text COLLATE utf8mb4_unicode_ci,
  `language_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `image`, `role`, `language_id`) VALUES
(1, 'admin', 'admin@admin.com', NULL, '$2y$10$zBec.fZxnV0QylVZDCZxYuG0tEr6MOMgT3p.r6jJfBEZk6TVtQSdS', 'eT28zW8cgF58IQt1n5Bds87zZB6oUNxYagKsfdwoG46vjyiSRrh0DoJ8oBYX', '2020-02-09 05:47:07', '2020-02-09 05:47:07', 'admin-image.png', NULL, NULL),
(2, 'Ashish Dulal', 'azizdulal.ad@gmail.com', NULL, '$2y$10$1EiIrsHvmfVVMGHFZ2wSguVULCZHyDra1wmLq/WyOlkXj.h.nsglK', NULL, '2020-02-13 11:04:25', '2020-02-13 11:04:25', 'admin-image.png', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `blogcategories`
--
ALTER TABLE `blogcategories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `landing_pages`
--
ALTER TABLE `landing_pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `newsletters`
--
ALTER TABLE `newsletters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `page_settings`
--
ALTER TABLE `page_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `staff`
--
ALTER TABLE `staff`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `blogcategories`
--
ALTER TABLE `blogcategories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `blogs`
--
ALTER TABLE `blogs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `landing_pages`
--
ALTER TABLE `landing_pages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `languages`
--
ALTER TABLE `languages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `newsletters`
--
ALTER TABLE `newsletters`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `page_settings`
--
ALTER TABLE `page_settings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `staff`
--
ALTER TABLE `staff`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
